/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import com.easwareapps.g2l.GestureLauncher;
import com.easwareapps.g2l.PermissionRequestActivity;
import com.easwareapps.libs.EAZipManager;
import com.easwareapps.g2l.R;
import com.easwareapps.g2l.utils.DBHelper;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class BackupRestoreFragment extends Fragment implements OnClickListener {
	private static final int FILE_SELECT_CODE = 0x000040;
	//private static final int FOLDER_SELECT_CODE = 0x000041;
	TextView backup = null;
	TextView restore = null;
	View root;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.backup_restore_layout, container,
				false);
		root = rootView.findViewById(R.id.root);
		backup = (TextView)rootView.findViewById(R.id.backup);
		restore = (TextView)rootView.findViewById(R.id.restore);
		backup.setOnClickListener(this);
		restore.setOnClickListener(this);
		return rootView;
	}

	@TargetApi(23)
	private boolean isStoragePermissionGranted(int code) {

		String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
		if(isPermissionGranted(permissions[0])) {
			return true;
		}
		getActivity().requestPermissions(permissions, code);
		return false;

	}

	@TargetApi(23)
	private boolean isPermissionGranted(String permission){
		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			return true;
		}else {
			int hasStoragePermission = getActivity().checkSelfPermission(permission);
			if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
				return false;

			}
		}
		return true;
	}


	@Override
	public void onClick(View v) {


		if (v == backup) {
			if(isStoragePermissionGranted(999)) {
				backup();
			}
		} else if (v == restore) {
			if(isStoragePermissionGranted(998)) {
				showFileChooser();
			}
		}


	}

	public void showFileChooser() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
		intent.setType("*/*"); 
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		try {
			startActivityForResult(
					Intent.createChooser(intent, getResources().getText(R.string.select_file)),
					FILE_SELECT_CODE);
		} catch (android.content.ActivityNotFoundException ex) {
			// Potentially direct the user to the Market with a Dialog
			Toast.makeText(getActivity(), getResources().getText(R.string.install_file_manager), 
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if(requestCode == FILE_SELECT_CODE){
				DBHelper dbh = DBHelper.getInstance(getActivity());
				dbh.clearDB();
				deleteTmpFolder();
				unzip(data);
				getActivity().finish();
				Intent gl = new Intent(getActivity(), GestureLauncher.class);
				startActivity(gl);

			}
		}

	}


	private void unzip(Intent data) {
		String path = data.getData().getEncodedPath();
		EAZipManager zipManager = new EAZipManager();
		String tmpPath = createTmpFolder();
		zipManager.unzip(path, tmpPath);
		try{
			restoreDatabase(tmpPath);
			restoreSharedPrefs(tmpPath);
			restoreGestures(tmpPath);
			delete(new File(tmpPath));
		}catch(Exception e){

		}

	}


//	public void selectBakupFolder(){
//		backup();
//	}


	private void restoreSharedPrefs(String path) {
		if(path != null){
			SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = g2lPref.edit();
			File f = new File(path, "prefs.xml");
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line;
				while ((line=br.readLine())!=null) {
					String[] prefs = line.split(",");
					if(prefs[1].equals("true")){
						editor.putBoolean(prefs[0], true);
					}else if(prefs[1].equals("false")){
						editor.putBoolean(prefs[0], false);
					}else{
						editor.putInt(prefs[0], Integer.parseInt(prefs[1]));
					}
				}
				br.close();
			} catch (Exception e) {

				e.printStackTrace();
			}
			editor.commit();


		}


	}

	private void restoreDatabase(String path) {
		if(path != null){
			DBHelper db = new DBHelper(getActivity());
			File f = new File(path, "db.xml");
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line;
				while ((line=br.readLine())!=null) {
					String[] cols = line.split(" , ");
					int id = Integer.parseInt(cols[0]);
					int action = Integer.parseInt(cols[1]);
					String option = cols[2].replace(" \\, ", " , ");
					String desc = cols[3];
					boolean confirm = false;
					if(cols[4].equals("true")){
						confirm = true;
					}
					Log.d("option", option);
					Log.d("desc", desc);
					db.addGesture(id, action, option, desc, confirm);

				}
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			db.close();


		}


	}

	@SuppressWarnings("rawtypes")
	private void restoreGestures(String path) {
		String appPath = getAppPath();
		copy(new File(path, "gestures"), new File(appPath, "gestures"));
		GestureLibrary gestureLib = GestureLibraries.fromFile(new File(path, "gestures"));
		gestureLib.load();

		Iterator iter = null;
		try{
			Set<String> gestureNames = gestureLib.getGestureEntries();
			iter = gestureNames.iterator();
		}catch(Exception E){
			return;
		}

		while (iter.hasNext()) {
			String gestureName = iter.next().toString();
			copy(new File(path, gestureName+".png"), new File(appPath, gestureName+".png"));
		}
		File customQuickLaunch = new File(path, "quick_launcher.png");
		if(customQuickLaunch.exists()) {
			copy(customQuickLaunch, new File(appPath, "quick_launcher.png"));
		}

	}

	private String getAppPath() {
		return new File(Environment.getExternalStorageDirectory().getAbsolutePath(), ".g2l").getAbsolutePath();

	}


	public void backup(){

		String path = createTmpFolder();
		try {
			backupSharePrefs(path);
			backupDatabase(path);
			backupGestures(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		File backupFile = new File(Environment.getExternalStorageDirectory(), "g2l_backup"+getDate()+".zip");
		EAZipManager zipManager = new EAZipManager();
		String[] files = getFilesIn(path);
		zipManager.zip(files, backupFile.getAbsolutePath(), path);
		try {
			delete(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Toast.makeText(getActivity(), getString(R.string.backuped, backupFile.getAbsolutePath()),
				Toast.LENGTH_LONG).show();

	}


	@SuppressWarnings("rawtypes")
	private void backupGestures(String tmpPath) {
		String appPath = getAppPath();
		copy(new File(appPath, "gestures"), new File(tmpPath, "gestures"));
		GestureLibrary gestureLib = GestureLibraries.fromFile(new File(appPath, "gestures"));
		gestureLib.load();

		Iterator iter = null;
		try{
			Set<String> gestureNames = gestureLib.getGestureEntries();
			iter = gestureNames.iterator();
		}catch(Exception E){
			return;
		}

		while (iter.hasNext()) {
			String gestureName = iter.next().toString();
			copy(new File(appPath, gestureName+".png"), new File(tmpPath, gestureName+".png"));
		}
		File customQuickLaunch = new File(appPath, "quick_launcher.png");
		if(customQuickLaunch.exists()) {
			copy(customQuickLaunch, new File(tmpPath, "quick_launcher.png"));
		}

	}


	private void backupSharePrefs(String path) {

		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path, "prefs.xml")));
			String name = "";
			int value;
			boolean ans;

			/***********************************Quick Launch************************************/

			name = "enable_quick_launch";
			ans = g2lPref.getBoolean(name, true);
			writer.write(name+","+ans+"\n");

			name = "quicklauncher_alpha";
			value = g2lPref.getInt(name, 128);
			writer.write(name+","+value+"\n");

			name  = "quicklauncher_size";
			value = g2lPref.getInt(name, 1);
			writer.write(name+","+value+"\n");

			name = "quicklauncher_position";
			value = g2lPref.getInt(name, 0);
			writer.write(name+","+value+"\n");

			name = "quicklauncher_position_x";
			value = g2lPref.getInt(name,0);
			writer.write(name+","+value+"\n");

			name = "quicklauncher_position_y";
			value = g2lPref.getInt(name, -100);
			writer.write(name+","+value+"\n");

			name = "custom_quick_launch_icon";
			ans = g2lPref.getBoolean(name, false);
			writer.write(name+","+ans+"\n");

			/**************************************swipe launch settings*****************************/

			name = "enable_swipe_launch";
			ans = g2lPref.getBoolean(name, true);
			writer.write(name+","+ans+"\n");

			name = "swipe_launch_position";
			value = g2lPref.getInt(name, 1);
			writer.write(name+","+value+"\n");
			
			name = "swipe_type";
			value = g2lPref.getInt(name, 1);
			writer.write(name+","+value+"\n");

			/**************************************gesture settings*****************************/

			name = "enable_multiple_strokes";
			ans = g2lPref.getBoolean(name, true);
			writer.write(name+","+ans+"\n");

			name = "stroke_fade_time";
			value = g2lPref.getInt(name, 2);
			writer.write(name+","+value+"\n");

			name = "gesture_sensitivity";
			value = g2lPref.getInt(name, 2);
			writer.write(name+","+value+"\n");

			name = "finish_activity_after_launch";
			ans = g2lPref.getBoolean(name, true);
			writer.write(name+","+ans+"\n");

			name = "gesture_color";
			value = g2lPref.getInt(name, Color.YELLOW);
			writer.write(name+","+value+"\n");


			/**************************************other settings*****************************/

			name = "manual_confirmation";
			ans = g2lPref.getBoolean(name, false);
			writer.write(name+","+ans+"\n");

			name = "auto_launch_wait_time";
			value = g2lPref.getInt(name, 5);
			writer.write(name+","+value+"\n");
			
			
			name = "show_toast";
			ans = g2lPref.getBoolean(name, true);
			writer.write(name+","+ans+"\n");
			
			name = "donated";
			ans = g2lPref.getBoolean(name, false);
			writer.write(name+","+ans+"\n");




			writer.close();
		} catch (IOException e) {

			e.printStackTrace();
		} 

	}

	private void backupDatabase(String path) throws Exception{

		DBHelper db = new DBHelper(getActivity());
		Cursor gestures = db.getAllGestureDetails();
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path, "db.xml")));
		if(gestures.moveToFirst()){
			do {
				int id = gestures.getInt(0);
				int action = gestures.getInt(1);
				String option = gestures.getString(2).replace(" , ", "\\,");
				String desc = gestures.getString(3).replace(" , ", " \\, ");
				String confirm = gestures.getString(4);
				writer.write(id+" , "+action+" , "+option+" , "+desc+" , "+confirm+"\n");
			}while (gestures.moveToNext());
		}
		gestures.close();
		db.close();
		writer.close();

	}

	private String createTmpFolder(){

		File tmpDir = new File(Environment.getExternalStorageDirectory(), ".tmp.com.easwareapps.g2l");
		if(!tmpDir.exists()){
			tmpDir.mkdir();
		}
		return tmpDir.getAbsolutePath();
	}

	private void deleteTmpFolder(){

		File tmpDir = new File(Environment.getExternalStorageDirectory(), ".tmp.com.easwareapps.g2l");
		if(tmpDir.exists()){
			tmpDir.delete();
		}
	}

	private void copy(File source, File destination){
		InputStream inStream = null;
		OutputStream outStream = null;

		try{


			inStream = new FileInputStream(source);
			outStream = new FileOutputStream(destination);

			byte[] buffer = new byte[1024];

			int length;
			//copy the file content in bytes 
			while ((length = inStream.read(buffer)) > 0){

				outStream.write(buffer, 0, length);

			}

			inStream.close();
			outStream.close();

			System.out.println("File is copied successful!");

		}catch(IOException e){
			e.printStackTrace();
		}
	}

	void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	private String[] getFilesIn(String path) {
		String[] files = new String[10000];
		File tmpPath = new File(path);
		int index = 0;
		for(File f: tmpPath.listFiles()){
			files[index] = f.getAbsolutePath();
			index++;
		}
		String compactArray[] = new String[index];
		System.arraycopy(files, 0, compactArray, 0, index);
		return compactArray;
	}

	private String getDate() {
		try{
			Calendar cal = Calendar.getInstance();
			String date = "-" + DateFormat.format("yyyy-MM-dd_HH-mm-ss", cal).toString();
			return date;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";


	}

}

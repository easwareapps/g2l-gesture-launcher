/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;

import com.easwareapps.g2l.PermissionRequestActivity;
import com.easwareapps.g2l.R;


public class OtherSettingsFragment extends Fragment
		implements OnSeekBarChangeListener,
			OnCheckedChangeListener {


	SeekBar seekWait = null;
	TextView txtWaitTime = null;
	Switch chkEnableToast = null;
	TextView txtConfirmationStyle;
	TextView txtChangeTheme;
	TextView txtSetVoiceAssist;
	AlertDialog themeChooserDialog;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.other_settings, container,
				false);

		seekWait = (SeekBar)rootView.findViewById(R.id.seekAutoConfirmationWaitTime);
		txtWaitTime = (TextView)rootView.findViewById(R.id.txtWaitTime);
		txtConfirmationStyle = (TextView)rootView.findViewById(R.id.txtConfirmationStyle);
		txtChangeTheme = (TextView) rootView.findViewById(R.id.chageTheme);
		chkEnableToast = (Switch) rootView.findViewById(R.id.chkEnableToast);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			View divider = rootView.findViewById(R.id.dividerVoiceAssist);
			divider.setVisibility(View.VISIBLE);
			txtSetVoiceAssist = (TextView) rootView.findViewById(R.id.txtSetVoiceAssist);
			txtSetVoiceAssist.setVisibility(View.VISIBLE);
			txtSetVoiceAssist.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					startActivity(new Intent(Settings.ACTION_VOICE_INPUT_SETTINGS));
				}
			});
		}


		seekWait.setOnSeekBarChangeListener(this);
		chkEnableToast.setOnCheckedChangeListener(this);
		TextView fixPermissions = (TextView) rootView.findViewById(R.id.fixPermissionIssues);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			fixPermissions.setVisibility(View.VISIBLE);
			fixPermissions.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent permissionRequest = new Intent(getActivity(),
							PermissionRequestActivity.class);
					startActivity(permissionRequest);
				}
			});
		} else {
			fixPermissions.setVisibility(View.GONE);
		}

		initUI();
		
		return rootView;
	}

	private void initUI() {
		
		final SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		final boolean state = g2lPref.getBoolean("manual_confirmation", false);
		seekWait.setEnabled(!state);
		
		int time = g2lPref.getInt("auto_launch_wait_time", 5);
		seekWait.setProgress(time-1);
		
		txtWaitTime.setText(String.format(getResources().getString(R.string.auto_confirmation_wait_time), time + ""));
		
		chkEnableToast.setChecked(g2lPref.getBoolean("show_toast", true));
		txtConfirmationStyle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				AlertDialog.Builder showConfirmationStyleChooser
						= new AlertDialog.Builder(new ContextThemeWrapper(
						getActivity(), getCurrentTheme()));
				showConfirmationStyleChooser.setSingleChoiceItems(
						getResources().getStringArray(R.array.confirmation_styles),
						g2lPref.getBoolean("manual_confirmation", false) ? 1 : 0,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								boolean state = i==0?false:true;
								changeAutoConfirmation(state);
							}
						}
				);
				showConfirmationStyleChooser.show();
			}
		});
		txtChangeTheme.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final AlertDialog.Builder themeChooser
						= new AlertDialog.Builder(new ContextThemeWrapper(
						getActivity(), getCurrentTheme()));

				themeChooser.setSingleChoiceItems(
						getResources().getStringArray(R.array.themes),
						g2lPref.getBoolean("enable_dark_theme", false) ? 1 : 0,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								boolean state = i==0?false:true;
								SharedPreferences.Editor editor = g2lPref.edit();
								editor.putBoolean("enable_dark_theme", state);
								editor.apply();
								changeTheme();


							}
						}
				);
				themeChooserDialog = themeChooser.create();
				themeChooserDialog.show();

			}
		});
		
		
	}

	private void changeTheme() {
		themeChooserDialog.dismiss();
		getActivity().recreate();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = g2lPref.edit();
		editor.putInt("auto_launch_wait_time", progress+1);
		txtWaitTime.setText(String.format(getResources().getString(R.string.auto_confirmation_wait_time), (progress+1) + ""));
		editor.commit();
		

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCheckedChanged(CompoundButton view, boolean isChecked) {
		if(view == chkEnableToast){
			changeToastMessage(isChecked);
		}

	}

	private void changeAutoConfirmation(boolean state) {
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = g2lPref.edit();
		editor.putBoolean("manual_confirmation", state);
		editor.commit();
		seekWait.setEnabled(!state);
	}
	
	private void changeToastMessage(boolean state) {
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = g2lPref.edit();
		editor.putBoolean("show_toast", state);
		editor.commit();
	}

	private int getCurrentTheme() {
		SharedPreferences pref = getActivity().getSharedPreferences(getActivity().getPackageName(),
									Context.MODE_PRIVATE);
		return pref.getBoolean("enable_dark_theme", false) ? R.style.Dialog_Dark:
				R.style.Dialog_Light;
	}
}

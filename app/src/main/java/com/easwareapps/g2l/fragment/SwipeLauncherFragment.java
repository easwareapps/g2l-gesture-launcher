/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import com.easwareapps.g2l.PermissionRequestActivity;
import com.easwareapps.g2l.service.FloatingWidgetService;
import com.easwareapps.g2l.service.FloatingWidgetService.LocalBinder;
import com.easwareapps.g2l.R;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class SwipeLauncherFragment extends Fragment
		implements OnCheckedChangeListener, View.OnClickListener {

	Switch chkEnableSwipeLaunch = null;
	TextView swipeLaunchPosition;
	TextView swipeLaunchType;


	boolean isConnected;
	FloatingWidgetService floatingWidgetServer;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.swipe_launcher_settings, container, false);

		chkEnableSwipeLaunch = (Switch) rootView.findViewById(R.id.chkEnableSwipeLaunch);
		swipeLaunchPosition = (TextView) rootView.findViewById(R.id.swipeLaunchPosition);
		swipeLaunchType = (TextView) rootView.findViewById(R.id.swipeType);



		initFloatingWidgetConnection();
		initGUIObjects();

		chkEnableSwipeLaunch.setOnCheckedChangeListener(this);
		swipeLaunchPosition.setOnClickListener(this);
		swipeLaunchType.setOnClickListener(this);

		return rootView;
	}

	private void initFloatingWidgetConnection() {
		Intent serviceIntent = new Intent(getActivity(), FloatingWidgetService.class);
		getActivity().bindService(serviceIntent, floatingWidgetConnection, Context.BIND_AUTO_CREATE);

	}


	private void initGUIObjects() {
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);

		chkEnableSwipeLaunch.setChecked(g2lPref.getBoolean("enable_swipe_launch", true));
		int pos = g2lPref.getInt("swipe_launch_position", 0);


		int type = g2lPref.getInt("swipe_type", 1);


	}


	@Override
	public void onCheckedChanged(CompoundButton view, boolean isChecked) {

		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor prefEditor = g2lPref.edit();
		if(view == chkEnableSwipeLaunch){
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isChecked) {
				if (!Settings.canDrawOverlays(getActivity())) {
					Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
							Uri.parse("package:" + getActivity().getPackageName()));
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivityForResult(intent,
							PermissionRequestActivity.REQ_OVERLAY_PERMISSION);
				}
			}
			prefEditor.putBoolean("enable_swipe_launch", isChecked);
			prefEditor.commit();
			if(floatingWidgetServer == null){
				Intent intent = new Intent(getActivity(), FloatingWidgetService.class);
				getActivity().startService(intent);
				getActivity().bindService(intent, floatingWidgetConnection, Context.BIND_AUTO_CREATE);
				if(floatingWidgetServer != null){
					floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
				}
			}else{
				floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
			}
			return;
		}

		if(isConnected){
			floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
		}

	}


	ServiceConnection floatingWidgetConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			isConnected = false;
			floatingWidgetServer = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			isConnected = true;
			LocalBinder localBinder = (LocalBinder)service;
			floatingWidgetServer = localBinder.getServerInstance();
		}



	};





	@Override
	public void onClick(View view) {

		final SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		final SharedPreferences.Editor prefEditor = g2lPref.edit();
		if(view == swipeLaunchType) {
			AlertDialog.Builder swipeTypeChooser = new AlertDialog.Builder(new ContextThemeWrapper(
					getActivity(), getCurrentTheme()));
			swipeTypeChooser.setSingleChoiceItems(
					getResources().getStringArray(R.array.swipe_launch_types),
					g2lPref.getInt("swipe_type", 0),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							prefEditor.putInt("swipe_type", i);
							prefEditor.apply();
							if(isConnected) {
								floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
							}
						}
					});
			swipeTypeChooser.show();
		}else if(view == swipeLaunchPosition){
			AlertDialog.Builder swipeLaunchPositonChooser
					= new AlertDialog.Builder(new ContextThemeWrapper(
					getActivity(), getCurrentTheme()));
			swipeLaunchPositonChooser.setSingleChoiceItems(
					getResources().getStringArray(R.array.swipe_launch_positions),
					g2lPref.getInt("swipe_launch_position", 0),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							prefEditor.putInt("swipe_launch_position", i);
							prefEditor.apply();
							if(isConnected) {
								floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
							}

						}
					});
			swipeLaunchPositonChooser.show();

		}
	}

	private int getCurrentTheme() {
		SharedPreferences pref = getActivity().getSharedPreferences(getActivity().getPackageName(),
				Context.MODE_PRIVATE);
		return pref.getBoolean("enable_dark_theme", false) ? R.style.Dialog_Dark:
				R.style.Dialog_Light;
	}
}

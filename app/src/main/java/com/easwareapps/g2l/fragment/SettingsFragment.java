/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.easwareapps.g2l.DetailedSettings;
import com.easwareapps.g2l.R;

public class SettingsFragment extends Fragment {

	QuickLauncherFragment qlf = null;
	GestureSettingFragment gsf = null;
	SwipeLauncherFragment slf = null;
	OtherSettingsFragment osf = null;
	boolean twoPane = false;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.settings_layout, container,
				false);
		
		ListView settingsList = (ListView)rootView.findViewById(R.id.main_settings_list);
		if(rootView.findViewById(R.id.specific_settings_list) != null)
			twoPane = true;
		//settingsList.setAdapter(adapter)
		String[] settingsArray = getResources().getStringArray(R.array.settings_list);
		ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, 
				settingsArray);
		settingsList.setAdapter(listViewAdapter);
		settingsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {		
				changeDetailedSettingsFragment(position);
			}

		});
		
		return rootView;
	}
	
	private void changeDetailedSettingsFragment(int index) {
		// TODO Auto-generated method stub
		if(!twoPane){
			Intent detailedSettings = new Intent(getActivity(), DetailedSettings.class);
			detailedSettings.putExtra("settings_index", index);
			startActivity(detailedSettings);
			return;
		}
		switch (index) {
		case 0:
			if(qlf == null)
				qlf = new QuickLauncherFragment();
			getActivity().getSupportFragmentManager().beginTransaction().
				replace(R.id.specific_settings_list, qlf).commit();
				
			break;
		case 1:
			if(slf == null)
				slf = new SwipeLauncherFragment();
			getActivity().getSupportFragmentManager().beginTransaction().
				replace(R.id.settings_list, slf).commit();

			break;
		case 2:
			if(gsf == null)
				gsf = new GestureSettingFragment();
			getActivity().getSupportFragmentManager().beginTransaction().
				replace(R.id.specific_settings_list, gsf).commit();
				
			break;
		case 3:
			if(osf == null)
				osf = new OtherSettingsFragment();
			getActivity().getSupportFragmentManager().beginTransaction().
				replace(R.id.specific_settings_list, osf).commit();
				
			break;
		case 4:
			BackupRestoreFragment brf = new BackupRestoreFragment();
			gsf = new GestureSettingFragment();
			getActivity().getSupportFragmentManager().beginTransaction().
				replace(R.id.specific_settings_list, brf).commit();
			break;

		default:
			break;
		}
	}


}

/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import com.easwareapps.g2l.utils.BitmapManager;
import com.easwareapps.g2l.utils.BitmapManager.AsyncDrawable;
import com.easwareapps.g2l.R;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import static com.easwareapps.g2l.SelectActionActivity.REQUEST_CONTACT;

public class SelectContactFragment extends Fragment implements OnItemClickListener{

	ListView lv = null;
	Integer[] contactId;
	String[] contactNames;
	Drawable[] icons;
	LruCache<String, Bitmap> cache;
	int size = 150;
	
	private static final int TYPE_PHONE = 0;
	private static final int TYPE_EMAIL = 1;
	
	int selectedIndex = -1;
	String contactName = "";
	Bitmap nullImage;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		
		nullImage = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_contacts);
		
		View rootView = inflater.inflate(R.layout.specific_action_list, container,
				false);

		lv = (ListView)rootView.findViewById(R.id.specific_action_list);
		final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
		int cacheSize = maxMemory/8;
		cache = new LruCache<String, Bitmap>(cacheSize){

			@Override
			protected int sizeOf(String key, Bitmap value) {

				return value.getRowBytes() - value.getHeight(); 

			}

		};
		enableSearch();
		if(contactPermissionGranted()) {
			new LoadContacts().execute(new String[]{""});
		}
		return rootView;
	}

	@TargetApi(23)
	private boolean contactPermissionGranted() {

		String permissions[] = new String[]{Manifest.permission.READ_CONTACTS};
		if(isPermissionGranted(permissions[0])) {
			return true;
		}
		requestPermissions(permissions, REQUEST_CONTACT);
		return false;

	}

	@TargetApi(23)
	private boolean isPermissionGranted(String permission){
		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			return true;
		}else {
			int hasStoragePermission = getActivity().checkSelfPermission(permission);
			if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
				return false;

			}
		}
		return true;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		if (isPermissionGranted(Manifest.permission.READ_CONTACTS)) {
			new LoadContacts().execute(new String[]{""});
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}
	
	class LoadContacts extends AsyncTask<String, Void, Void>{

		@Override
		protected Void doInBackground(String... params) {
			getContactNames(params[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			lv.setAdapter(new ContactAdapter(getActivity()));
			enableSearch();
			super.onPostExecute(result);
		}
		
	}
	
	private void enableSearch(){
		final SearchView sv = (SearchView)getActivity().findViewById(R.id.menu_search);
		if(sv == null){Log.d("G2LMENU", "return");return;}
		sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
               new LoadContacts().execute(new String[]{s});
               if(s.equals("")){
            	   return false;
               }
               return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
		
		sv.setOnCloseListener(new OnCloseListener() {
			
			@Override
			public boolean onClose() {
				new LoadContacts().execute(new String[]{""});
				return false;
			}
		});
	}
	
	private void getContactNames(String search) {
		Log.d("SEARCH", search);
		String where = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " LIKE '" + "%" + search + "%'";
		String extra = "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC";
		String[] selectionArguments = null;
		
	    if(search.equals("")){
	    	where =null;
	    	selectionArguments = null;
	    }
		Cursor contacts = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, where,
			 	selectionArguments,  extra);
		contactId = new Integer[contacts.getCount()];
		contactNames = new String[contacts.getCount()];
		int i = 0;
		int nameFieldColumnIndex = contacts.getColumnIndex(PhoneLookup.DISPLAY_NAME);
		int idFieldColumnIndex = contacts.getColumnIndex(PhoneLookup._ID);
		while(contacts.moveToNext()) {

			String contactName = contacts.getString(nameFieldColumnIndex);
			contactNames[i] = contactName ; 
			contactId[i] = contacts.getInt(idFieldColumnIndex);

			i++;
		}

		contacts.close();
		lv.setOnItemClickListener(this);
		
	}
	
	public class ContactAdapter extends BaseAdapter{

		Context context = null;
		Bitmap nullImage = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_contacts);
		public ContactAdapter(Context c) {
			context = c;

		}
		@Override
		public int getCount() {
			return contactNames.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
		

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		    
			View view = convertView;
			int iconSize = getIconSize();
			int id =  contactId[position];
			if(view == null){
				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.contact_view, parent, false);
			}

			ImageView imageView = (ImageView)view.findViewById(R.id.contact_image);
			imageView.setLayoutParams(new LinearLayout.LayoutParams(iconSize, iconSize));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

			final String imageKey = String.valueOf(contactId);
			final Bitmap bitmap = cache.get(imageKey);
			if (bitmap != null) {
				imageView.setImageBitmap(bitmap);
			} else {    
				Resources res = context.getResources();
				BitmapManager bm = new BitmapManager(imageView, res, cache);
				bm.setType(BitmapManager.FROMCONTACT);
				bm.setContext(getActivity());
		        final AsyncDrawable asyncDrawable =
		                new AsyncDrawable(getResources(), nullImage, bm);
		        imageView.setImageDrawable(asyncDrawable);
		        bm.execute(id);

			}

			TextView txtAppName = (TextView)view.findViewById(R.id.contact_name);
			try{
				txtAppName.setText(contactNames[position]);
			}catch(Exception e){
				e.printStackTrace();
			}
			return view;
		}

	}

	String numbers[] = new String[10000];
	String typeName[] = new String[10000];
	Integer type[] = new Integer[10000];
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long ids) {
		
		Cursor contacts = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		if (contacts.moveToFirst()) {


			int id = contactId[position];

			//String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
			int i =0;
			//if (hasPhone.equalsIgnoreCase("1")) 
			{
				Cursor phones = getActivity().getContentResolver().query( 
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, 
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id, 
						null, null);
				
				if(phones.moveToFirst()){							
					do{
						try{
							numbers[i] = new String();
							typeName[i] = new String();
							type[i] = Integer.valueOf(TYPE_PHONE); 
							typeName[i] = "Unknown";
							numbers[i] = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							int phonetype = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
							String customLabel = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
							String phoneLabel = (String) ContactsContract.CommonDataKinds.Phone.getTypeLabel(this.getResources(), phonetype, customLabel);                       
							typeName[i] = phoneLabel;




							i++;
						}catch(Exception E){

						}
					}while(phones.moveToNext());							
				}
				Cursor emailCursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
						null,
						ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[] { id + "" }, null);
				if(emailCursor.moveToFirst()){

					do
					{

						numbers[i] = new String();
						typeName[i] = new String();
						type[i] = Integer.valueOf(TYPE_EMAIL);
						numbers[i] = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
						int emailType = emailCursor.getInt(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
						typeName[i] = (String) ContactsContract.CommonDataKinds.Email.getTypeLabel(this.getResources(), emailType, "");

						//Log.d("Email"m )
						i++;

					}while (emailCursor.moveToNext());
				}

				emailCursor.close();

			}
			try{
				String userNumbers[]= new String[i];
				for(int j = 0; j < i;j++){
					userNumbers[j] = numbers[j]+"\n"+typeName[j];

				}

				final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
				// set title
				alertDialogBuilder.setTitle("Select");

				// set dialog message
				alertDialogBuilder
				.setSingleChoiceItems(userNumbers, -1,  new OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						selectedIndex = arg1;

					}
				}).setCancelable(true);

				alertDialogBuilder.setPositiveButton(getString(R.string.title_message), new OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						seletPhoneNumber(arg1);
					}

				}).setNegativeButton(getString(R.string.title_call), new OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						seletPhoneNumber(arg1);

					}
				}).setNeutralButton(getString(R.string.title_email), new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						seletPhoneNumber(which);

					}
				});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

				
				contactName = contactNames[position]; 
				//Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();



			}catch(Exception E){
				System.out.print("EXCEPTION="+E);
			}

		}
	}
	
	private void seletPhoneNumber(int which) {
		
		int action = -4;
		String desc = "";
		if(which == -1){
			action = SelectSpecificActionFragment.MSG;
			desc = getString(R.string.message, contactName, typeName[selectedIndex]);
		}else if(which == -2){
			action = SelectSpecificActionFragment.CALL;
			desc =  getString(R.string.call, contactName, typeName[selectedIndex]);
		}else if(which == -3){
			if(type[selectedIndex] != TYPE_EMAIL){
				Toast errorToast = Toast.makeText(getActivity(),
						getString(R.string.please_select_email), Toast.LENGTH_SHORT);
				errorToast.setGravity(Gravity.CENTER, 0, 0);
				errorToast.show();
				return;
			}
			action = SelectSpecificActionFragment.MAIL;
			desc = getString(R.string.email, contactName, typeName[selectedIndex]);
		}
		if(selectedIndex!=-1){
			Intent databackIntent = new Intent();
			databackIntent.putExtra("action", action);
			databackIntent.putExtra("option", numbers[selectedIndex]);
			databackIntent.putExtra("desc", desc);
			getActivity().setResult(0, databackIntent);
			getActivity().finish();
		}
		
	}
	
	private int getIconSize(){
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		final int width = dm.widthPixels;
		return (width/4);
	}
}

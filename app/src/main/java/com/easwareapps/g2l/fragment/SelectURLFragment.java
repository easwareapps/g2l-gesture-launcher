/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import com.easwareapps.g2l.utils.BitmapManager;
import com.easwareapps.g2l.utils.BitmapManager.AsyncDrawable;
import com.easwareapps.g2l.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class SelectURLFragment extends Fragment implements OnItemClickListener {

	ListView lv = null;
	String[] urlNames = null;
	String[] bookmarks = null;
	byte[][] favicon = null;
	LruCache<String, Bitmap> cache;
	int size = 150;
	Integer[] urlId = new Integer[10000];


	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.specific_action_list, container,
				false);

		lv = (ListView)rootView.findViewById(R.id.specific_action_list);
		final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
		int cacheSize = maxMemory/8;
		cache = new LruCache<String, Bitmap>(cacheSize){

			@Override
			protected int sizeOf(String key, Bitmap value) {
				// TODO Auto-generated method stub

				return value.getRowBytes() - value.getHeight(); 

			}

		};
		getUrlList();
		return rootView;
	}

	
	private void getUrlList() {
		int i=0;
	    final int count = 1;
		urlNames = new String[count];
		bookmarks = new String[count];
		favicon = new byte[count][];
		bookmarks[i] = "Enter URL";
		
		lv.setAdapter(new UrlAdapter(getActivity()));
		lv.setOnItemClickListener(this);
		
	}

	public class UrlAdapter extends BaseAdapter{

		Context context = null;
		Bitmap nullImage = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_url);
		public UrlAdapter(Context c) {
			// TODO Auto-generated constructor stub
			context = c;

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return urlNames.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			int iconSize = getIconSize();
			View view = convertView;
			if(view == null){
				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.url_view, parent, false);
			}
			ImageView imageIcon = (ImageView)view.findViewById(R.id.favicon);
			imageIcon.setLayoutParams(new LinearLayout.LayoutParams(iconSize, iconSize));
			imageIcon.setScaleType(ImageView.ScaleType.CENTER_CROP);

			final String imageKey = String.valueOf(favicon[position]);
			final Bitmap bitmap = cache.get(imageKey);
			if (bitmap != null) {
				imageIcon.setImageBitmap(bitmap);
			} else {
				imageIcon.setImageBitmap(nullImage);
				Resources res = context.getResources();
				BitmapManager bm = new BitmapManager(imageIcon, res, cache);
				bm.setType(BitmapManager.FROMBLOB);
				bm.setContext(getActivity());
		        final AsyncDrawable asyncDrawable =
		                new AsyncDrawable(getResources(), nullImage, bm);
		        imageIcon.setImageDrawable(asyncDrawable);
		        bm.setFavicon(favicon[position]);
		        bm.execute(0);

			}



			TextView txtAppName = (TextView)view.findViewById(R.id.bookmark);
			try{
				txtAppName.setText(bookmarks[position]);
			}catch(Exception e){
				e.printStackTrace();
			}

			return view;
		}


	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		if(position == 0){
			getUrLFromuser();
		}else{
			returnURL(urlNames[position], bookmarks[position]);
		}		


	}

	private void getUrLFromuser() {

		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

		alert.setTitle("URL");
		alert.setMessage("Enter URL");

		// Set an EditText view to get user input 
		final EditText input = new EditText(getActivity());
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				String url = input.getText().toString();
				returnURL(url, url);
			}
		});
		alert.show();

	}

	private void returnURL(String url, String name) {
		if(!url.equals("")){
			if(name.equals("")){
				name = url;
			}
			Intent databackIntent = new Intent();
			databackIntent.putExtra("action", SelectSpecificActionFragment.URL);
			databackIntent.putExtra("option", url);
			databackIntent.putExtra("desc", "Open " + name);
			getActivity().setResult(0, databackIntent);
			getActivity().finish();
		}

	}
	
	private int getIconSize(){
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		final int width = dm.widthPixels;
		return (int)(width/4);
	}
}

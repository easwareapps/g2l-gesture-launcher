/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import com.easwareapps.g2l.GestureLauncher;
import com.easwareapps.g2l.PermissionRequestActivity;
import com.easwareapps.g2l.QLIconChangerActivity;
import com.easwareapps.g2l.service.FloatingWidgetService;
import com.easwareapps.g2l.service.FloatingWidgetService.LocalBinder;
import com.easwareapps.g2l.R;
import com.easwareapps.g2l.service.KeyboardWatcher;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;

public class QuickLauncherFragment extends Fragment implements OnCheckedChangeListener, OnSeekBarChangeListener, AdapterView.OnItemSelectedListener, View.OnClickListener {

	public static final int REQUEST_ICON_CHANGE = 555;
	Switch chkEnableQuickLaunch = null;
	Switch chkDisableQuickLaunchOnKB = null;
	TextView txtQuickLaunchPosition = null;
    TextView txtQLIconChanger = null;
	TextView txtKeyBoardActiveTitle = null;
	SeekBar seekQuickLaunchTransparency = null;
	SeekBar seekQuickLaunchSize = null;
	//Spinner spinnerQuickLaunchPosition = null;

	boolean isConnected;
	FloatingWidgetService floatingWidgetServer;
//	boolean iskwConnected;
//	KeyboardWatcher keyboardWatcher;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.quick_launcher_settings, container,
				false);

		chkEnableQuickLaunch = (Switch) rootView.findViewById(R.id.chkEnableQuickLaunch);
		chkDisableQuickLaunchOnKB = (Switch) rootView.findViewById(R.id.chkDisableQuickLaunchOnKeyBoard);
		txtQuickLaunchPosition = (TextView)rootView.findViewById(R.id.txtQuikLaunchPosition);
        txtQLIconChanger = (TextView)rootView.findViewById(R.id.txtChangeQLIcon);
		seekQuickLaunchTransparency = (SeekBar)rootView.findViewById(R.id.seekQuikLaunchTransperancy);
		seekQuickLaunchSize = (SeekBar)rootView.findViewById(R.id.seekQuickLaunhSize);
		txtKeyBoardActiveTitle = (TextView) rootView.findViewById(R.id.txtDisableQuickLaunchOnKeyBoard);

		

		initGuiObjects();
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
				Settings.canDrawOverlays(getActivity())) {
			initFloatingWidgetConnection();
		}

		chkEnableQuickLaunch.setOnCheckedChangeListener(this);
		chkDisableQuickLaunchOnKB.setOnCheckedChangeListener(this);

		
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2){
			chkDisableQuickLaunchOnKB.setVisibility(View.GONE);
			TextView txtDesc = (TextView)rootView.findViewById(R.id.txtDisableQuickLaunchOnKeyBoard);
			txtDesc.setVisibility(View.GONE);
		}

		return rootView;
	}

	private void initGuiObjects(){
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		boolean isQLEnabled =g2lPref.getBoolean("enable_quick_launch",true);
		chkEnableQuickLaunch.setChecked(isQLEnabled);
		onCheckedChanged(chkEnableQuickLaunch, isQLEnabled);
		
		boolean isDQLKEnabled =g2lPref.getBoolean("disable_quick_launch_on_kb", false);
		chkDisableQuickLaunchOnKB.setChecked(isDQLKEnabled && isNLServiceRunning());
		

		int position = 0;
		try{
			position = g2lPref.getInt("quicklauncher_position", 0);
		}catch(Exception e){
			position = 0;
			e.printStackTrace();
		}
//		spinnerQuickLaunchPosition.setSelection(position);
//		spinnerQuickLaunchPosition.setOnItemSelectedListener(this);
		seekQuickLaunchSize.setOnSeekBarChangeListener(this);
		seekQuickLaunchTransparency.setOnSeekBarChangeListener(this);
		txtQuickLaunchPosition.setOnClickListener(this);
		txtKeyBoardActiveTitle.setOnClickListener(this);
        txtQLIconChanger.setOnClickListener(this);
		
		int progress = g2lPref.getInt("quicklauncher_size", 0);
		seekQuickLaunchSize.setProgress(progress);
		
		progress = g2lPref.getInt("quicklauncher_alpha", 255);
		seekQuickLaunchTransparency.setProgress(progress);

	}

	private void initFloatingWidgetConnection() {
		Intent serviceIntent = new Intent(getActivity(), FloatingWidgetService.class);
		getActivity().bindService(serviceIntent, floatingWidgetConnection, Context.BIND_AUTO_CREATE);

	}

	@Override
	public void onCheckedChanged(CompoundButton view, boolean isChecked) {
		if(view == chkEnableQuickLaunch){
			SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
			SharedPreferences.Editor g2lEditor =g2lPref.edit();
			g2lEditor.putBoolean("enable_quick_launch",isChecked);
			g2lEditor.commit();
			if(isChecked){
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					if (!Settings.canDrawOverlays(getActivity())) {
						Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
								Uri.parse("package:" + getActivity().getPackageName()));
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivityForResult(intent,
								PermissionRequestActivity.REQ_OVERLAY_PERMISSION);
					}
				}
			}
			txtQuickLaunchPosition.setEnabled(isChecked);
			seekQuickLaunchTransparency.setEnabled(isChecked);
			seekQuickLaunchSize.setEnabled(isChecked);
			if(floatingWidgetServer == null){
				Intent i = new Intent(getActivity(), FloatingWidgetService.class);
				getActivity().startService(i);
				try{
					getActivity().bindService(i, floatingWidgetConnection, Context.BIND_AUTO_CREATE);
				}catch(Exception e){
					
				}
				if(floatingWidgetServer != null){
					floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
				}
			}else{
				floatingWidgetServer.adjustQuickLauncherTypeAndPosition();
			}
		}else if(chkDisableQuickLaunchOnKB == view){
			if(isChecked && !isNLServiceRunning()){
				Intent intent=new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
				startActivity(intent);

			}
			if(isChecked && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				if(!isKeyoardWatcherRunning()) {
					Intent keyboardWatcherIntent = new Intent(getActivity(),
							KeyboardWatcher.class);
					getActivity().startService(keyboardWatcherIntent);
				}
			}

			if(isChecked) {
				AlertDialog.Builder builder =
				 new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), getCurrentTheme()));
				builder.setTitle(R.string.info);
				builder.setMessage(R.string.disable_ql_info);
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				});
				builder.create().show();
			}

			SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
			SharedPreferences.Editor g2lEditor =g2lPref.edit();
			g2lEditor.putBoolean("disable_quick_launch_on_kb", isChecked);
			g2lEditor.commit();
			
			
		}
	}

	private boolean isKeyoardWatcherRunning() {
		ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			Log.d("Service", service.service.getClassName());
			if ("KeyboardWatcher".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}


	ServiceConnection floatingWidgetConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			isConnected = false;
			floatingWidgetServer = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			isConnected = true;
			LocalBinder localBinder = (LocalBinder)service;
			floatingWidgetServer = localBinder.getServerInstance();
		}


	};



	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		Log.d("SEEK", progress+"");
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor g2lEditor =g2lPref.edit();
		if(seekBar == seekQuickLaunchSize){
			g2lEditor.putInt("quicklauncher_size", progress);
		}else if(seekBar == seekQuickLaunchTransparency){
			g2lEditor.putInt("quicklauncher_alpha", progress);
		}
		g2lEditor.commit();
		if(floatingWidgetServer != null){
			floatingWidgetServer.setQuickLaunchPosition();
		}
		
	}

	public void updateQuickLauncher() {
		if(floatingWidgetServer != null){
			floatingWidgetServer.setQuickLaunchPosition();
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		
	}
	
	private boolean isNLServiceRunning() {
		ComponentName cn = new ComponentName(getActivity(), KeyboardWatcher.class);
		String flat = Settings.Secure.getString(getActivity().getContentResolver(), "enabled_notification_listeners");
		final boolean enabled = flat != null && flat.contains(cn.flattenToString());
		return enabled;
    }

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor g2lEditor =g2lPref.edit();
		g2lEditor.putInt("quicklauncher_position", i);
		g2lEditor.apply();
		if(isConnected) {
			floatingWidgetServer.adjustQuickLauncherTypeAndPosition();;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	private int getCurrentTheme() {
		SharedPreferences pref = getActivity().getSharedPreferences(getActivity().getPackageName(),
				Context.MODE_PRIVATE);
		return pref.getBoolean("enable_dark_theme", false) ? R.style.Dialog_Dark:
				R.style.Dialog_Light;
	}



	@Override
	public void onClick(View view) {

		final SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);


		if(view == txtQuickLaunchPosition) {
			AlertDialog.Builder positionChooser = new AlertDialog.Builder(new ContextThemeWrapper(
					getActivity(), getCurrentTheme()));
			String positions[] = getResources().getStringArray(R.array.quick_launcher_positions);
			positionChooser.setSingleChoiceItems(positions,
							g2lPref.getInt("quicklauncher_position",
							0), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					SharedPreferences.Editor g2lEditor =g2lPref.edit();
					g2lEditor.putInt("quicklauncher_position", i);
					g2lEditor.apply();
					if(isConnected) {
						floatingWidgetServer.adjustQuickLauncherTypeAndPosition();;
					}

				}
			});
			positionChooser.show();
		} else if(view == txtKeyBoardActiveTitle) {
			AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
					getActivity(), getCurrentTheme()));
			final EditText input = new EditText(getActivity());
			builder.setTitle(R.string.keyboard_active_notification_title);
			input.setText(g2lPref.getString("keyboard_active_notification", "Choose keyboard"));
			input.setMaxLines(1);
			input.setHint(R.string.keyboard_active_notification_title);
			builder.setView(input);

			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					SharedPreferences.Editor g2lEditor =g2lPref.edit();
					g2lEditor.putString("keyboard_active_notification", input.getText().toString());
					g2lEditor.apply();

				}
			});
			builder.create().show();

		} else  if (view == txtQLIconChanger) {
            Intent iconChanger = new Intent(getActivity(), QLIconChangerActivity.class);
            getActivity().startActivityForResult(iconChanger, REQUEST_ICON_CHANGE);
        }
	}
}

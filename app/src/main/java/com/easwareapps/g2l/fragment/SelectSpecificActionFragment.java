/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.easwareapps.g2l.R;
import com.easwareapps.g2l.utils.DataTransfer;

public class SelectSpecificActionFragment extends Fragment {

	public static final int APPS = 0; 
	public static final int WIFI = 1;
	public static final int BLUETOOTH = 2;
	public static final int DATA_CONNECTIVITY = 3;
	public static final int SYNC = 4;
	public static final int SOUND = 5;
	public static final int BRIGHTNESS = 6;
	public static final int LED_FLASH = 7;
	public static final int URL = 8;
	public static final int OTHER = 9;
	public static final int CALL_MSG_MAIL = 10;
	
	public static final int FILE_SELECT = 0x000036;
	public static final int OPEN_FILE = 0x000037;
	public static final int CALL = 0x000038;
	public static final int MSG = 0x000039;
	public static final int MAIL = 0x000035;
	
	

	LruCache<String, Bitmap> cache;
	int size = 150;
	int mainAction = -1;
	String subAction = "";
	String description = "";



	ListView lv = null;
	SelectAppFragment appfragment = null;
	String[] actionList;
	
	String settings[] = {"",
			"com.android.settings,com.android.settings.wifi.WifiSettings", 
			"com.android.settings,com.android.settings.bluetooth.BluetoothSettings",
			"com.android.settings,com.android.settings.Settings$DataUsageSummaryActivity",
			"com.android.settings,com.android.settings.accounts.SyncSettingsActivity",
			"com.android.settings,com.android.settings.Settings$SoundSettingsActivity",
			"com.android.settings,com.android.settings.Settings$DisplaySettingsActivity",
			"com.android.settings,com.android.settings.Settings$LocationSettingsActivity",
			"com.android.settings,com.android.settings.Settings$WirelessSettingsActivity",
			"com.android.settings,com.android.settings.fuelgauge.PowerUsageSummary",
			"com.android.settings,com.android.settings.TetherSettings",
			};
	
	String desc[];

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.specific_action_list, container,
				false);

		desc = getResources().getStringArray(R.array.open_settings_desc);
		lv = (ListView)rootView.findViewById(R.id.specific_action_list);
		final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
		int cacheSize = maxMemory/8;
		cache = new LruCache<String, Bitmap>(cacheSize){

			@Override
			protected int sizeOf(String key, Bitmap value) {
				// TODO Auto-generated method stub

				return value.getRowBytes() - value.getHeight(); 

			}

		};

		rootView.findViewById(R.id.notice).setVisibility(View.GONE);
		mainAction = DataTransfer.itemSelected;
		switch (mainAction) {
		case WIFI:
			actionList = getResources().getStringArray(R.array.wifi_action_list);
			break;
		case BLUETOOTH:
			actionList = getResources().getStringArray(R.array.bluetooth_action_list);
			break;
		case DATA_CONNECTIVITY:
			actionList = getResources().getStringArray(R.array.data_action_list);
			if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
				actionList = getResources().getStringArray(R.array.data_action_list_lollipop);
				rootView.findViewById(R.id.notice).setVisibility(View.VISIBLE);
			}
			break;
		case SYNC:
			actionList = getResources().getStringArray(R.array.sync_action_list);
			break;
		case SOUND:
			actionList = getResources().getStringArray(R.array.sound_action_list);
			break;
		case BRIGHTNESS:
			actionList = getResources().getStringArray(R.array.brightness_action_list);
			break;
		case LED_FLASH:
			actionList = getResources().getStringArray(R.array.flash_action_list);
			break;
		case OTHER:
			actionList = getResources().getStringArray(R.array.other_action_list);
			break;
		case CALL_MSG_MAIL:
			break;
		default:
			actionList = getResources().getStringArray(R.array.action_list);
			break;
		}

		ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_list_item_1, 
				actionList);
		lv.setAdapter(listViewAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				subAction = "" + position;
				if(mainAction == DATA_CONNECTIVITY){
					if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
						subAction = "" + 3;
					}
				}
				if(actionList.length == position + 1 && mainAction < 7){
					subAction = settings[mainAction];
					mainAction = APPS;
				}else if(mainAction == OTHER){
					if(position != 4){
						subAction = settings[mainAction+position-2];
						mainAction = APPS;
					}else{
						Intent fileSelect = new Intent(Intent.ACTION_GET_CONTENT);
						fileSelect.setType("file/*");
						startActivityForResult(fileSelect, FILE_SELECT);
						return;
					}
				}
				
				
				Intent databackIntent = new Intent();
				databackIntent.putExtra("action", mainAction);
				databackIntent.putExtra("option", subAction);
				databackIntent.putExtra("desc", actionList[position]);
				getActivity().setResult(0, databackIntent);
				getActivity().finish();
			}
		});


		return rootView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FILE_SELECT){
			try{
				String filePath = "";
				filePath = data.getData().getEncodedPath();
				if(filePath.startsWith("file://")){
					filePath = filePath.replace("file://", "");
				}
				Intent databackIntent = new Intent();
				databackIntent.putExtra("action", OPEN_FILE);
				databackIntent.putExtra("option", filePath);
				databackIntent.putExtra("desc", "Open File");
				getActivity().setResult(0, databackIntent);
				getActivity().finish();
			}catch(Exception E){
				E.printStackTrace();
			}

		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}

}

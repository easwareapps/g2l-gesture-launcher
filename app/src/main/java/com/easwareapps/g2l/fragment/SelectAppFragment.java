/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import java.util.Collections;
import java.util.List;

import com.easwareapps.g2l.R;
import com.easwareapps.g2l.utils.BitmapManager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class SelectAppFragment extends Fragment implements OnItemClickListener {

	ListView lv = null;
	String[] appNames = null;
	String[] packageList = null;
	Drawable[] icons = null;
	LruCache<String, Bitmap> cache;
	int size = 150;
	Integer[] appId = new Integer[10000];
	Bitmap nullImage;
	LoadApps loadApps;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		nullImage = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_app);
		
		View rootView = inflater.inflate(R.layout.specific_action_list, container,
				false);

		lv = (ListView)rootView.findViewById(R.id.specific_action_list);
		final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
		int cacheSize = maxMemory/8;
		cache = new LruCache<String, Bitmap>(cacheSize){

			@Override
			protected int sizeOf(String key, Bitmap value) {
				// TODO Auto-generated method stub

				return value.getRowBytes() - value.getHeight(); 

			}

		};
		String params[] = {""};
		loadApps = new LoadApps();
		loadApps.execute(params);
		return rootView;
	}
	
	
	class LoadApps extends AsyncTask<String, Void, Void>{

		@Override
		protected Void doInBackground(String... params) {
			getAppList(params[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			lv.setAdapter(new AppsAdapter(getActivity()));
			super.onPostExecute(result);
		}
		
	}
	private void getAppList(String search) {
		appNames = new String[10000];
		packageList = new String[10000];
		icons = new Drawable[10000];

		final PackageManager pm = getActivity().getPackageManager();

		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		final List<ResolveInfo> packages = pm.queryIntentActivities(
				mainIntent, 0);
		Collections.sort(packages, new ResolveInfo.DisplayNameComparator(
				pm));

		//List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		int i=0;
		for (ResolveInfo packageInfo : packages) {
			appNames[i] = new String(packageInfo.loadLabel(pm).toString());
			if(!search.equals("")){
				if(!appNames[i].toUpperCase().contains(search.toUpperCase())){
					continue;
				}
			}
			appId[i] = Integer.valueOf(i);
			icons[i] = packageInfo.loadIcon(pm);
			packageList[i] = new String(packageInfo.activityInfo.packageName + "," + packageInfo.activityInfo.name);
			i++;
		}
		String compactAppName[] = new String[i];
		System.arraycopy(appNames, 0, compactAppName, 0, i);

		Drawable d[] = new Drawable[i];
		System.arraycopy(icons, 0, d, 0, i);

		icons = d; 

		appNames = compactAppName;
		lv.setOnItemClickListener(this);
		enableSearch();
	}
	
	private void enableSearch(){
		final SearchView sv = (SearchView)getActivity().findViewById(R.id.menu_search);
		if(sv == null)return;
		sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
               new LoadApps().execute(new String[]{s});
               if(s.equals("")){
            	   return false;
               }
               return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
		
		sv.setOnCloseListener(new OnCloseListener() {
			
			@Override
			public boolean onClose() {
				new LoadApps().execute(new String[]{""});
				return false;
			}
		});
	}
	
	public class AppsAdapter extends BaseAdapter{

		Context context = null;
		Bitmap nullImage = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_app);
		public AppsAdapter(Context c) {
			// TODO Auto-generated constructor stub
			context = c;

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return appNames.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int iconSize = getIconSize();
			View view = convertView;
			if(view == null){
				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.app_view, parent, false);
			}
			ImageView imageIcon = (ImageView)view.findViewById(R.id.app_icon);
			imageIcon.setLayoutParams(new LinearLayout.LayoutParams(iconSize, iconSize));
			imageIcon.setScaleType(ImageView.ScaleType.CENTER_CROP);

			final String imageKey = String.valueOf(appId);
			final Bitmap bitmap = cache.get(imageKey);
			if (bitmap != null) {
				imageIcon.setImageBitmap(bitmap);
			} else {
				Resources res = context.getResources();
				BitmapManager bm = new BitmapManager(imageIcon, res, cache);
				bm.setContext(getActivity());
				bm.setType(BitmapManager.FROMPACKAGE);
				bm.setPackageName(packageList[position], icons[position]);
				final BitmapManager.AsyncDrawable asyncDrawable =
		                new BitmapManager.AsyncDrawable(getResources(), nullImage, bm);
		        imageIcon.setImageDrawable(asyncDrawable);
				bm.execute(position);
			}



			TextView txtAppName = (TextView)view.findViewById(R.id.app_name);
			try{
				txtAppName.setText(appNames[position]);
			}catch(Exception e){
				e.printStackTrace();
			}

			return view;
		}


	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		Intent databackIntent = new Intent();
		databackIntent.putExtra("action", SelectSpecificActionFragment.APPS);
		databackIntent.putExtra("option", packageList[position]);
		databackIntent.putExtra("desc", "Open " + appNames[position]);
		getActivity().setResult(0, databackIntent);
		getActivity().finish();
		
		
	}
	
	private int getIconSize(){
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		final int width = (dm.widthPixels > dm.heightPixels)?dm.heightPixels:dm.widthPixels;
		return width/5;
	}
	
}

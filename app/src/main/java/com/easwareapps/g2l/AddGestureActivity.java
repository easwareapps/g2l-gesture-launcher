/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.easwareapps.g2l.utils.DBHelper;
import com.easwareapps.g2l.utils.DataTransfer;
import com.easwareapps.g2l.utils.GestureInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AddGestureActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener, View.OnClickListener {

    int action = GestureInfo.NO_ACTION;
    String option = "";
    String desc = "";
    GestureOverlayView gov;
    GestureLibrary gestureLib;
    String appPath;
    Gesture userGesture;
    int SELECT_ACTION = 0;

    Button btnSelectAction;
    Button btnSaveGesture;
    ImageView btnClearGesture;
    CheckBox chkConfirmation;
    boolean editGesture;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
                R.style.G2LTheme_NoActionBar ;
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_gesture);

        
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null) {
            toolbar.setTitle(R.string.title_add_gesture);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationIcon(R.mipmap.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

        gov = (GestureOverlayView) findViewById(R.id.gestureOverlayView1);
        gov.setFadeOffset(100000000);
        gov.addOnGesturePerformedListener(this);

        SharedPreferences g2lPref = getSharedPreferences(getPackageName(),
                                        Activity.MODE_PRIVATE);

        if(g2lPref.getBoolean("enable_multiple_strokes", true)){
            gov.setGestureStrokeType(GestureOverlayView.GESTURE_STROKE_TYPE_MULTIPLE);

        }else{
            gov.setGestureStrokeType(GestureOverlayView.GESTURE_STROKE_TYPE_SINGLE);
        }
        int color = g2lPref.getInt("gesture_color", Color.YELLOW);
        gov.setGestureColor(color);

        btnSelectAction = (Button) findViewById(R.id.idBtnSelectAction);
        btnSelectAction.setOnClickListener(this);

        btnSaveGesture = (Button) findViewById(R.id.idBtnSave);
        btnSaveGesture.setOnClickListener(this);


        btnClearGesture = (ImageView) findViewById(R.id.idClear);
        btnClearGesture.setOnClickListener(this);

        chkConfirmation = (CheckBox) findViewById(R.id.idConfirmationBeforeLaunch);


        if(DataTransfer.gestureName != -1){
            initGesture();
            try {
                setGesture(DataTransfer.gestureName);
            } catch (Exception e) {

            }
            editGesture = true;
        }else{
            editGesture = false;
        }

    }

    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {

    }

    private void initGesture(){
        File appDir = new File(Environment.getExternalStorageDirectory(),".g2l");
        if(!(appDir.exists() && appDir.isDirectory())){
            appDir.mkdir();
        }
        appPath = appDir.getAbsolutePath();
        File storeFile = new File(appPath, "gestures");
        if(!storeFile.exists()){
            try {
                storeFile.createNewFile();
            } catch (IOException e) {
                Log.e("FILE CREATION", e.getLocalizedMessage());
            }
        }
        gestureLib = GestureLibraries.fromFile(storeFile);
        gestureLib.load();
    }

    @Override
    public void onClick(View v) {

        if(v == btnSelectAction) {
            Intent intentSelectAction =
                    new Intent(getApplicationContext(), SelectActionActivity.class);
            startActivityForResult(intentSelectAction, SELECT_ACTION);
        } else if(v == btnSaveGesture) {
            saveGestureDetails();
        } else if (v == btnClearGesture) {
            clearGesture();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

    }

    public void saveGestureDetails(){
        boolean confirmation = chkConfirmation.isChecked();
        if (action != GestureInfo.NO_ACTION && !option.equals("")) {
            DBHelper db = DBHelper.getInstance(getApplicationContext());
            int id = db.getNexGestureId();
            if(editGesture){
                db.deleteGesture(DataTransfer.gestureName);
            }
            db.addGesture(id, action, option, desc, confirmation);
            initGesture();
            saveImage("" + id);
            saveGesture("" + id);
            finish();
        } else{
            Snackbar.make(findViewById(R.id.activity_add_gesture),
                    R.string.gesture_option_empty_msg, Snackbar.LENGTH_LONG).show();
        }

    }

    private void saveImage(String name) {

        try {
            FileOutputStream out = new FileOutputStream(new File(appPath, name +".png"));
            gov.getGesture().toBitmap(300, 300, 1, Color.YELLOW).compress(Bitmap.CompressFormat.PNG,
                    90, out);
            out.close();
        } catch (Exception e) {
            Log.e("Save img", e.getLocalizedMessage());
        }

    }

    private void saveGesture(String name) {
        userGesture = gov.getGesture();
        gestureLib.addGesture(name, userGesture);
        gestureLib.save();

    }


    private void clearGesture(){
        userGesture = null;
        gov.cancelClearAnimation();
        gov.clear(true);
    }

    private void setGesture(int name) throws  Exception{
        ArrayList<Gesture> p = gestureLib.getGestures(name+"");
        if(p == null)
            return;;
        if(p.size()>0){
            userGesture = p.get(0);

            try{
                int WIDTH,HEIGHT;
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();

                display.getSize(size);
                WIDTH = size.x;
                HEIGHT = size.y;


                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                    int tmp = WIDTH;
                    WIDTH = HEIGHT;
                    HEIGHT = tmp;
                }
                gov.setLeft(-WIDTH);
                gov.setTop(-WIDTH);

            }catch(Exception e){
               Log.e("SET GESTURE", e.getLocalizedMessage());
            }

            gov.setGesture(userGesture);

            DBHelper dbh = DBHelper.getInstance(getApplicationContext());
            Cursor c = dbh.getGestures(name);
            if(c.moveToFirst()){
                action = c.getInt(1);
                option = c.getString(2);
                desc = c.getString(3);
                if(c.getString(4).equals("true")){
                    chkConfirmation.setChecked(true);
                }else{
                    chkConfirmation.setChecked(false);
                }
            }

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            action = data.getIntExtra("action", -4);
            option = data.getStringExtra("option");
            desc = data.getStringExtra("desc");
        }catch(Exception e){

        }
        super.onActivityResult(requestCode, resultCode, data);
    }



}

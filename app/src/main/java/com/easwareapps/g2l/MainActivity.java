/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */package com.easwareapps.g2l;

import android.Manifest;
import android.annotation.TargetApi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.easwareapps.g2l.adapter.GestureAdapter;
import com.easwareapps.g2l.utils.DBHelper;
import com.easwareapps.g2l.utils.GestureInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public static final int RESULT_STOP = -999;
    RecyclerView gestureList;
    GestureLibrary gestureLib;
    ArrayList<GestureInfo> gestures = new ArrayList<>();
    int themeUsed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        int theme = getCurrentTheme();
        themeUsed = theme;
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null) {
            toolbar.setNavigationIcon(R.mipmap.ic_launcher);
            setSupportActionBar(toolbar);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        gestureList = (RecyclerView) findViewById(R.id.gestureList);
        //gestureList.addItemDecoration(new MarginDecoration(this));
        gestureList.setHasFixedSize(true);
        gestureList.setLayoutManager(new GridLayoutManager(this, getGridColumns()));
        loadGestureList();

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addGesture = new Intent(getApplicationContext(),
                        AddGestureActivity.class);
                startActivityForResult(addGesture, 1000);
            }
        });

        gestureList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    //fab.setVisibility(View.GONE);
                    View rv = findViewById(R.id.root);
                    fab.animate().translationY(fab.getHeight() + rv.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
                } else if (dy < 0){
                    //fab.setVisibility(View.VISIBLE);
                    fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();

                }
            }
        });

    }

    public void loadGestureList() {
        new LoadGestures().execute();
    }


    public void deleteGesture(int id) {
        DBHelper dbh = DBHelper.getInstance(getApplicationContext());
        dbh.deleteGesture(id);
        gestureLib.removeEntry(id + "");
        gestureLib.save();
        removeGestureImage(id + "");
    }

    class LoadGestures extends AsyncTask<Void, Void, Void> {

        ArrayList<GestureInfo> newGestures;
        boolean reload = false;
        @Override
        protected Void doInBackground(Void... voids) {
            newGestures = getGestures();
            if(newGestures.size() != gestures.size()) {
                gestures = newGestures;
                reload = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
//            if (reload){
                GestureAdapter ga = GestureAdapter.getInstance(getApplicationContext(),
                        getGridSize(), gestureList,
                        MainActivity.this, gestures);
                gestureList.setAdapter(ga);
//            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {

            Intent settings = new Intent(this, SettingsActivity.class);
            startActivityForResult(settings, 1000);
        } else if (id == R.id.nav_donate) {

            Intent donate = new Intent(this, DonateActivity.class);
            startActivity(donate);
        } else if (id == R.id.nav_share) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            share.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            share.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.g2l_promote_msg));
            startActivity(share);

        } else if (id == R.id.nav_rate) {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW);
            rateIntent.setData(Uri.
                    parse("https://play.google.com/store/apps/details?" +
                            "id=com.easwareapps.g2l"));
            startActivity(rateIntent);

        } else if(id == R.id.nav_translate) {
            Intent translateIntent = new Intent(Intent.ACTION_VIEW);
            translateIntent.setData(Uri.
                    parse("http://translate.easwareapps.com?app=g2l"));
            startActivity(translateIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private int getGridSize() {

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        final int width = dm.widthPixels;
        int cols = getGridColumns();

        return width/cols;
    }

    private int getGridColumns() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float scalefactor = getResources().getDisplayMetrics().density * 150;
        int number = dm.widthPixels;
        int columns = (int) ((float) number / (float) scalefactor);
        return columns;

    }
    private File getDefaultDirectory() {
        File appPath = new File(Environment.getExternalStorageDirectory(), ".g2l");
        return appPath;
    }


    public ArrayList<GestureInfo> getGestures() {
        ArrayList<GestureInfo> gestures = new ArrayList<>();
        if(checkStoragePermission()) {
            File appPath = new File(Environment.getExternalStorageDirectory(), ".g2l");
            if (!appPath.exists()) {
                if (!appPath.mkdirs()) {
                    return gestures;
                }
            }
            File storeFile = new File(appPath, "gestures");
            if(!storeFile.exists()) {
                try {
                    storeFile.createNewFile();
                }catch (Exception e) {

                }
            }
            gestureLib = GestureLibraries.fromFile(storeFile);
            gestureLib.load();

            Set<String> gestureNames = gestureLib.getGestureEntries();
            Iterator iter = gestureNames.iterator();
            DBHelper dbh = DBHelper.getInstance(getApplicationContext());
            ArrayList<String> tobeRemoved = new ArrayList<>();
            while(iter.hasNext()){
                int id = Integer.parseInt(iter.next().toString());
                String description = dbh.getActionOption(id);
                if(description.equals("")){
                    tobeRemoved.add(id + "");
                } else {
                    GestureInfo gesture = new GestureInfo(id, description);
                    gestures.add(gesture);
                }


            }
            for(String gid:tobeRemoved) {
                gestureLib.removeEntry(gid);
                removeGestureImage(gid);
            }
            gestureLib.save();


        }
        return gestures;
    }

    private void removeGestureImage(String gid) {
        File image = new File(getDefaultDirectory(), gid);
        if(image.exists()){
            image.delete();
        }
    }

    @TargetApi(23)
    private boolean checkStoragePermission() {

        String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(isPermissionGranted(permissions[0])) {
            return true;
        }
        requestPermissions(permissions, 11);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (isPermissionGranted(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            loadGestureList();
        }else {
            Snackbar.make(findViewById(R.id.root), R.string.storage_permission_required,
                    Snackbar.LENGTH_LONG).setAction(R.string.fix_permission_issues,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent permissionFix = new Intent(getApplicationContext(),
                                    PermissionRequestActivity.class);
                            startActivityForResult(permissionFix, 1);
                        }
                    }).show();

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void sendMail(View v) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "info@easwareapps.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "FEEDBACK G2L");
        startActivity(Intent.createChooser(emailIntent,  getString(R.string.send_feedback)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if(requestCode == 1000) {
            if (themeUsed != getCurrentTheme()) {
                MainActivity.this.recreate();
                return;
            }
        }
        loadGestureList();
        super.onActivityResult(requestCode, resultCode, data);
    }



    private int getCurrentTheme() {
        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        return pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
                R.style.G2LTheme_NoActionBar ;
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0,1,0,"Edit");
        menu.add(0,2,0,"Delete");
        menu.add(0,3,0,"Cancel");

    }
}

package com.easwareapps.g2l.utils;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    SQLiteDatabase db = null;
    static final String DBNAME = "db_g2l";
    static final int VERSION = 7;

    private static final String TABLE_GESTURES = "g2l_gestures";
    private static final String TABLE_SETTINGS = "g2l_settings";
    private static final String KEY_ID = "gesture_id";
    private static final String KEY_ACTION = "gesture_action";
    private static final String KEY_OPTION = "gesture_option";
    private static final String KEY_DESC = "gesture_description";
    private static final String KEY_CONFIRMATION = "gesture_confirmation";
    Context context;
    private static DBHelper instance;

    public static DBHelper getInstance(Context context) {
        if(instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }
    public DBHelper(Context context) {
        super(context, DBNAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DB", "ONCREATE");
        try{
            String CREATE_TABLE = "CREATE TABLE " + TABLE_GESTURES + "("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ACTION + " INT,"
                    + KEY_OPTION + " TEXT," + KEY_DESC + " TEXT, " + KEY_CONFIRMATION + " VARCHAR(10))";
            db.execSQL(CREATE_TABLE);
        }catch(Exception E){

        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion <= 5){
            Log.d("upgrade", "upgrading....");
            try{
                backupGesture(db);
                backupAllSettings(db);
            }catch(Exception e){
                e.printStackTrace();
            }
            try{
                db.execSQL("DROP TABLE IF EXISTS '" + TABLE_GESTURES +"'");
                db.execSQL("DROP TABLE IF EXISTS '" + TABLE_SETTINGS +"'");
            }catch(Exception e){
                e.printStackTrace();
            }
            try{
                onCreate(db);
                restoreGestures(db);
                restoreSettings();
                deleteTmpFolder();
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            Log.d("upgrade", "Old version is higher");
        }
        //context.deleteDatabase(TABLE_GESTURES);
        //onCreate(db);

    }

    private void restoreSettings() {
        String path = createTmpFolder();
        if(path != null){
            SharedPreferences g2lPref = context.getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = g2lPref.edit();
            File f = new File(path, "prefs.xml");
            try {
                BufferedReader br = new BufferedReader(new FileReader(f));
                String line;
                while ((line=br.readLine())!=null) {
                    String[] prefs = line.split(",");
                    if(prefs[1].equals("true")){
                        editor.putBoolean(prefs[0], true);
                    }else if(prefs[1].equals("false")){
                        editor.putBoolean(prefs[0], false);
                    }else{
                        editor.putInt(prefs[0], Integer.parseInt(prefs[1]));
                    }
                }
                br.close();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            editor.commit();


        }


    }

    public void clearDB() {

        db = this.getWritableDatabase();
        db.delete(TABLE_GESTURES, null, null);
        db.close();
    }

    private void backupAllSettings(SQLiteDatabase oldDB) throws Exception {

        String dir = createTmpFolder();
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dir, "prefs.xml")));
        String query = "SELECT * from g2l_settings";
        Cursor cursor = oldDB.rawQuery(query, null);
        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                String name = cursor.getString(1);
                String value = cursor.getString(2);
                if(name.equals("quicklauncher_position")){
                    try{
                        int pos = Integer.parseInt(value);
                        if(pos<=2){
                            value = ""+(pos+6);
                        }else if(pos>=5 && pos <=7){
                            value = ""+(pos-4);
                        }else if(pos==8){
                            value = "0";
                        }
                    }catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                writer.write(name+","+value+"\n");
                cursor.moveToNext();
            }
            cursor.close();
        }
        writer.close();

    }

    private String createTmpFolder(){

        File tmpDir = new File(Environment.getExternalStorageDirectory(), ".tmp.com.easwareapps.g2l");
        if(!tmpDir.exists()){
            tmpDir.mkdir();
        }
        return tmpDir.getAbsolutePath();
    }

    private void deleteTmpFolder(){

        File tmpDir = new File(Environment.getExternalStorageDirectory(), ".tmp.com.easwareapps.g2l");
        if(tmpDir.exists()){
            tmpDir.delete();
        }
    }

    private void backupGesture(SQLiteDatabase oldDB) throws IOException {

        String dir = createTmpFolder();
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dir, "db.xml")));
        String query = "SELECT * from " + TABLE_GESTURES;
        Cursor cursor = oldDB.rawQuery(query, null);
        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                int id = cursor.getInt(0);
                String[] values = getNewActionOption(cursor.getString(1), cursor.getString(2));
                String action = values[0];
                String option = values[1];
                String desc = cursor.getString(3).replace(",", "\\,");
                String confirm = cursor.getString(5).replace(",", "\\,");
                writer.write(id+" , "+action+" , "+option+" , "+desc+" , "+confirm+"\n");
                cursor.moveToNext();
            }
            cursor.close();
        }
        writer.close();

    }

    private String[] getNewActionOption(String action, String option) {

        String newValues[] =new String[2];
        int ac = Integer.parseInt(action);
        newValues[1] = option;
        switch(ac){
            case 4:newValues[0] = GestureInfo.APPS + "";break;
            case 5:newValues[0] = GestureInfo.MSG + "";break;
            case 6:newValues[0] = GestureInfo.CALL + "";break;
            case 7:newValues[0] = GestureInfo.OPEN_FILE + "";break;
            case 8:newValues[0] = GestureInfo.URL + "";break;
            case 9:newValues[0] = GestureInfo.MAIL + "";break;
        }
        if(ac < 4){
            if(option.equals("Bluetooth")){
                newValues[0] = GestureInfo.BLUETOOTH + "";
            }else if(option.equals("Wifi")){
                newValues[0] = GestureInfo.WIFI + "";
            }else if(option.equals("Data")){
                newValues[0] = GestureInfo.DATA_CONNECTIVITY + "";
            }else if(option.equals("Sync")){
                newValues[0] = GestureInfo.SYNC + "";
            }else if(option.equals("Brightness")){
                newValues[0] = GestureInfo.BRIGHTNESS + "";
            }else if(option.equals("Sound")){
                newValues[0] = GestureInfo.SOUND + "";
            }else if(option.equals("Flash")){
                newValues[0] = GestureInfo.LED_FLASH + "";
            }
            switch (ac) {
                case 0:
                    newValues[1] = GestureInfo.DISABLE + "";
                    break;
                case 1:
                    newValues[1] = GestureInfo.ENABLE + "";
                    break;
                case 2:
                    newValues[1] = GestureInfo.TOGGLE + "";
                    break;
                default:
                    newValues[1] = ac + "";
                    break;
            }
        }
        return newValues;
    }

    public boolean addGesture(int id, int action, String option, String desc, boolean b) {
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, id);
        values.put(KEY_ACTION, action);
        values.put(KEY_OPTION, option);
        values.put(KEY_DESC, desc);
        values.put(KEY_CONFIRMATION, b+"");

        try{
            db.insert(TABLE_GESTURES, null, values);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        db.close(); // Closing database connection
        return false;

    }

    public boolean addGestureFromDBH(int id, int action, String option, String desc, boolean b, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(KEY_ID, id);
        values.put(KEY_ACTION, action);
        values.put(KEY_OPTION, option);
        values.put(KEY_DESC, desc);
        values.put(KEY_CONFIRMATION, b+"");

        try{
            db.insert(TABLE_GESTURES, null, values);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        db.close(); // Closing database connection
        return false;

    }

    public int getNexGestureId(){
        db = getReadableDatabase();
        String selectQuery = "SELECT  max(" + KEY_ID + ") FROM " + TABLE_GESTURES;
        Cursor cursor = db.rawQuery(selectQuery, null);
        int next = 0;
        if(cursor.moveToFirst()){
            next = cursor.getInt(0);
        }
        cursor.close();
        next++;
        db.close();
        return next;
    }

    public String getActionOption(int id) {
        db = this.getReadableDatabase();
        String selectQuery = "SELECT  " + KEY_DESC + " FROM " + TABLE_GESTURES + " where "+ KEY_ID + " = "+id;
        Cursor cursor = db.rawQuery(selectQuery, null);
        String val = "";
        if(cursor.moveToFirst()){;
            val = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return val;

    }

    public Cursor getGestures(int id) {
        // TODO Auto-generated method stub
        db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_GESTURES + " where "+ KEY_ID + " = "+id;
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;

    }

    public void deleteGesture(int id) {
        // TODO Auto-generated method stub
        db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_GESTURES + " where "+KEY_ID+" = "+id;
        //System.out.print(query);
        db.execSQL(query);
        db.close();


    }

    public Cursor getAllGestureDetails() {
        db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_GESTURES;
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    private void restoreGestures(SQLiteDatabase db) throws Exception{

        File f = new File(createTmpFolder(), "db.xml");
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line=br.readLine())!=null) {
                String[] cols = line.split(" , ");
                int id = Integer.parseInt(cols[0]);
                int action = Integer.parseInt(cols[1]);
                String option = cols[2].replace(" \\, ", " , ");
                String desc = cols[3];
                boolean confirm = false;
                if(cols[4].equals("true")){
                    confirm = true;
                }
                Log.d("option", option);
                Log.d("desc", desc);
                addGestureFromDBH(id, action, option, desc, confirm, db);

            }
            br.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
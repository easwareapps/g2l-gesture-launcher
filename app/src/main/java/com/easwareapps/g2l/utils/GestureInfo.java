package com.easwareapps.g2l.utils;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GestureInfo {

    public static final int TOGGLE = 0;
    public static final int ENABLE = 1;
    public static final int DISABLE = 2;

    public static final int APPS = 0;
    public static final int WIFI = 1;
    public static final int BLUETOOTH = 2;
    public static final int DATA_CONNECTIVITY = 3;
    public static final int SYNC = 4;
    public static final int SOUND = 5;
    public static final int BRIGHTNESS = 6;
    public static final int LED_FLASH = 7;
    public static final int URL = 8;
    public static final int OTHER = 9;
    public static final int CALL_MSG_MAIL = 10;

    public static final int FILE_SELECT = 0x000036;
    public static final int OPEN_FILE = 0x000037;
    public static final int CALL = 0x000038;
    public static final int MSG = 0x000039;
    public static final int MAIL = 0x000035;
    public static final int NO_ACTION = -4;


    private int id;
    private String title;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private boolean selected;


    public GestureInfo(int id, String title) {
        this.id = id;
        this.title = title;
        selected = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }




}

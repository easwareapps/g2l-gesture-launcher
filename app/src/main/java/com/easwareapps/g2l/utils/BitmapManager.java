package com.easwareapps.g2l.utils;

import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import com.easwareapps.g2l.R;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class BitmapManager  extends AsyncTask<Integer, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;
    private int res = 0;
    Resources resource = null;
    LruCache<String, Bitmap> cache = null;
    int type = 0;
    private Context context = null;
    public static final int FROMRESOURCE = 0;
    public final static int FROMSTORAGE = 2;
    public static final int FROMPACKAGE = 4;
    public static final int FROMCONTACT = 3;
    public static final int FROMBLOB = 5;
    int posision = -1;
    int data = 0;
    byte[] favicon = null;

    String packageName = "";
    Drawable icon;


    public BitmapManager(ImageView imageView, Resources r, LruCache<String , Bitmap> cache) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        this.cache = cache;
        imageViewReference = new WeakReference<ImageView>(imageView);
        resource = r;
    }

    public void setContext(Context c){
        context = c;
    }

    public void setType(int t){
        type = t;
    }

    public void setPackageName(String name, Drawable d){
        packageName = name;
        icon = d;
    }

    public void setFavicon(byte[] b){
        favicon = b;
    }

    @Override
    protected Bitmap doInBackground(Integer... params) {
        res = params[0];
        data =res;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 4;
        options.inJustDecodeBounds = false;
        if(type == FROMRESOURCE){
            return BitmapFactory.decodeResource(resource, res, options);
        }else if(type == FROMSTORAGE){
            final File appDir = new File(Environment.getExternalStorageDirectory(), ".g2l");
            String sep = File.separator;
            String path = appDir + sep + params[0]+".png";
            return BitmapFactory.decodeFile(path);
        }else if(type == FROMPACKAGE){
            try {

                //Drawable icon = context.getPackageManager().getApplicationIcon(packageName);
                Bitmap bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                icon.draw(canvas);
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }else if(type == FROMCONTACT){

            return loadContactPhoto(context.getContentResolver(), res, options);
        }else if(type == FROMBLOB){

            return loadFavicon();
        }else{

        }
        return null;
    }


    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            final BitmapManager bitmapWorkerTask =
                    getBitmapManager(imageView);
            if (this == bitmapWorkerTask && imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }



    public Bitmap loadContactPhoto(ContentResolver cr, long  id, Options options) {

        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_contacts);
        }
        return BitmapFactory.decodeStream(input);
    }

    public Bitmap loadFavicon() {

        if(favicon != null){
            return BitmapFactory.decodeByteArray(favicon, 0, favicon.length);
        }
        return null;
    }


    private static BitmapManager getBitmapManager(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView) {
        final BitmapManager bitmapWorkerTask = getBitmapManager(imageView);

        if (bitmapWorkerTask != null) {
            final int bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == 0 || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }


    public static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapManager> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             BitmapManager bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapManager>(bitmapWorkerTask);
        }

        public BitmapManager getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }


}

/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.service;
import com.easwareapps.g2l.service.FloatingWidgetService.LocalBinder;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.widget.Toast;


@SuppressLint("NewApi")
public class KeyboardWatcher extends NotificationListenerService {

	Context context;
	boolean isConnected;
	FloatingWidgetService floatingWidgetServer;




	@Override
	public void onCreate() {
		super.onCreate();
		SharedPreferences g2lPref = getApplicationContext().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
//		if(!g2lPref.getBoolean("disable_quick_launch_on_kb", false)){
//
//			stopSelf();
//			return;
//		}
		

		context = getApplicationContext();
		try{
			Intent i = new Intent(getApplicationContext(), FloatingWidgetService.class);
			getApplicationContext().startService(i);
			try{
				getApplicationContext().bindService(i, floatingWidgetConnection, Context.BIND_AUTO_CREATE);
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	@Override
	public void onNotificationPosted(StatusBarNotification sbn) {
		SharedPreferences g2lPref = getApplicationContext().getSharedPreferences(getPackageName(),
				Context.MODE_PRIVATE);
		if(!g2lPref.getBoolean("disable_quick_launch_on_kb", false)) {
			return;
		}
		String title =     sbn.getNotification().extras.getString("android.title");
		if(title.equalsIgnoreCase("Choose input method") ||
				title.equalsIgnoreCase("Change keyboard")
				|| title.equalsIgnoreCase("Choose keyboard")
				|| title.equalsIgnoreCase(
					g2lPref.getString("keyboard_active_notification", "Choose keyboard")
					)
				) {
			if(floatingWidgetServer!=null)
				floatingWidgetServer.changeFloatingIcon(false);
		}

	}
	@Override
	public void onNotificationRemoved(StatusBarNotification sbn) {
		String title =     sbn.getNotification().extras.getString("android.title");
		if(title.equals("Choose input method") || title.equals("Change keyboard")
				|| title.equals("Choose keyboard")) {
			if(floatingWidgetServer!=null)
				floatingWidgetServer.changeFloatingIcon(true);
		}


	}

	ServiceConnection floatingWidgetConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			isConnected = false;
			floatingWidgetServer = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			isConnected = true;
			LocalBinder localBinder = (LocalBinder)service;
			floatingWidgetServer = localBinder.getServerInstance();
		}



	};
}

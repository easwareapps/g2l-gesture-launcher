/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Locale;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.easwareapps.g2l.service.FloatingWidgetService;
import com.easwareapps.g2l.service.KeyboardWatcher;
import com.easwareapps.g2l.utils.DBHelper;
import com.easwareapps.g2l.utils.FlashSettingns;
import com.easwareapps.g2l.utils.GestureInfo;


public class GestureLauncher extends AppCompatActivity
        implements OnGesturePerformedListener, OnClickListener {

    public static final int TOGGLE = 0;
    public static final int ENABLE = 1;
    public static final int DISABLE = 2;
    public static final int REQ_CAMERA = 444;
    public static final int REQUEST_STORAGE = 445;

    GestureLibrary gestureLib = null;
    GestureOverlayView gov = null;
    String appPath = "";
    File storeFile = null;
    ImageButton imgSettings = null;
    String PHONE_NO = "";
    String FLASH_STATE;


    boolean mBounded = false;
    //FloatingWidgetService mServer;
    ImageView imgArrow;
    TextView txtHint;
    DBHelper dbh = null;
    SharedPreferences g2lPref = null;
    Thread progressThread = null;
    boolean canceled = false;
    boolean waitingForPermission = false;
    boolean isOverlayEnaled;



    @TargetApi(23)
    protected void onCreate(Bundle savedInstanceState) {

        try{
            super.onCreate(savedInstanceState);
            initialize();
            setContentView(R.layout.gesture_launcher);
            imgArrow = (ImageView)findViewById(R.id.idImgArrow);
            txtHint = (TextView)findViewById(R.id.idHint);
            gov = (GestureOverlayView)findViewById(R.id.gestureOverlayView);
            imgSettings = (ImageButton)findViewById(R.id.idBtnSettings);

            g2lPref = this.getSharedPreferences(getPackageName(), MODE_PRIVATE);




            gov.addOnGesturePerformedListener(this);

            if(isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                File storeFile = new File(appPath, "gestures");
                if (!storeFile.exists()) {
                    try {
                        storeFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (gestureLib != null) {

                }
                gestureLib = GestureLibraries.fromFile(storeFile);
                gestureLib.load();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE);
            }
            int color;
            try{
                DBHelper db = new DBHelper(getApplicationContext());
                try{
                    if(db.getNexGestureId()!=1){
                        txtHint.setVisibility(View.INVISIBLE);
                        imgArrow.setVisibility(View.INVISIBLE);
                    }
                    db.close();
                }catch(Exception E){

                    db.close();
                }
                if(g2lPref.getBoolean("enable_multiple_strokes", true)){
                    gov.setGestureStrokeType(GestureOverlayView.GESTURE_STROKE_TYPE_MULTIPLE);
                    int fadetime = (g2lPref.getInt("stroke_fade_time", 2)+1)*250;
                    gov.setFadeOffset(fadetime);
                }else{
                    gov.setGestureStrokeType(GestureOverlayView.GESTURE_STROKE_TYPE_SINGLE);
                }

            }catch (Exception e) {

            }
            color = g2lPref.getInt("gesture_color", Color.YELLOW);
            gov.setGestureColor(color);

        }catch(Exception E){
            E.printStackTrace();
        }

        try{
            startServices();
        } catch (Exception e) {
            e.printStackTrace();
        }





    }


    private void startServices() {
        if(Build.VERSION.SDK_INT >= 23) {
            final SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
            if(!Settings.canDrawOverlays(getApplicationContext())) {
                isOverlayEnaled = false;
                if(!pref.getBoolean("donot_request_app_overlay", false)) {

                    Log.e("INSIDE", "1");

                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
                            this, getCurrentTheme()));
                    builder.setTitle(R.string.confirm);
                    builder.setMessage(R.string.enable_draw_over_screen);
                    builder.setPositiveButton(R.string.ok, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivityForResult(intent,
                                    PermissionRequestActivity.REQ_OVERLAY_PERMISSION);
                        }
                    }).setNegativeButton(R.string.cancel, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                        }
                    }).setNeutralButton(R.string.donot_ask, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean("donot_request_app_overlay", true);
                            editor.apply();

                        }
                    });
                    builder.show();




                }
                return;
            }
        }
        try {
            startService();
        } catch (Exception e) {

        }

        isOverlayEnaled = true;
    }


    @Override
    protected void onStart() {
        if(!waitingForPermission){
            super.onStart();
        }
        if(!isOverlayEnaled && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                Settings.canDrawOverlays(getApplicationContext())) {
            startServices();
            isOverlayEnaled = true;
        }
        super.onStart();

    }

    private void initialize() throws Exception{

        final File prevAppDir = new File(Environment.getExternalStorageDirectory(), "g2l");
        final File appDir = new File(Environment.getExternalStorageDirectory(), ".g2l");
        if(!appDir.exists() && prevAppDir.exists()){
            prevAppDir.renameTo(appDir);
        }
        else if(!(appDir.exists() && appDir.isDirectory())){
            appDir.mkdir();
        }
        appPath = appDir.getAbsolutePath();

    }

    public void startG2L(View view) {

        Intent i=new Intent(GestureLauncher.this, MainActivity.class);
        startActivityForResult(i, 0);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PermissionRequestActivity.REQ_OVERLAY_PERMISSION) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(getApplicationContext())) {
                    startActivity(new Intent(this, GestureLauncher.class));
                }
            }
        }
        if(requestCode > 1000) {
            try {
                android.provider.Settings.System.putInt(getApplicationContext().getContentResolver(),
                        android.provider.Settings.System.SCREEN_BRIGHTNESS, requestCode - 1000);
            }catch (Exception e) {

            }
            return;
        }

        int color = g2lPref.getInt("gesture_color", Color.YELLOW);
        gov.setGestureColor(color);

        try{
            gestureLib = GestureLibraries.fromFile(storeFile);
            gestureLib.load();
        }catch(Exception e){

        }
    }

    public void startService() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
                if(!(pref.getBoolean("enable_quick_launch", true) ||
                        pref.getBoolean("enable_swipe_launch", true)) ) {
                    return;
                }
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                        !Settings.canDrawOverlays(getApplicationContext())) {
                        return;
                }
                try{
                    Intent serviceIntent = new Intent(GestureLauncher.this, FloatingWidgetService.class);
                    startService(serviceIntent);
                }catch(Exception E){
                }
                try{
                    try{
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
                            Intent keyboardWatcherIntent = new Intent(GestureLauncher.this,
                                    KeyboardWatcher.class);
                            startService(keyboardWatcherIntent);
                            Log.d("Listen KB", "started......");
                        }
                    }catch(Exception E){
                        Log.d("service", E.toString());

                    }
                } catch(Exception E) {

                }
            }
        }).start();



    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        waitingForPermission = false;
        File storeFile = new File(appPath, "gestures");
        Log.d("PATH", storeFile.getAbsolutePath());
        if(!storeFile.exists()){
            try {
                storeFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        gestureLib = GestureLibraries.fromFile(storeFile);
        gestureLib.load();
        try{
            ArrayList<Prediction> predictions = gestureLib.recognize(gesture);
            if (predictions.size() > 0) {
                dbh = new DBHelper(getApplicationContext());
                int sensitivity = g2lPref.getInt("gesture_sensitivity", 2);
                Prediction prediction = null;
                int k=0;
                for(;k<predictions.size();k++){
                    prediction = predictions.get(k);
                    if(gesture.getStrokesCount() == gestureLib.getGestures(prediction.name).get(0).getStrokesCount() && prediction.score >= sensitivity){
                        break;
                    }
                }
                if(k == predictions.size()){
                    Toast message = Toast.makeText(this, R.string.no_matching_gesture, Toast.LENGTH_SHORT);
                    message.setGravity(Gravity.CENTER, 0, 0);
                    message.show();
                    return;
                }
                Cursor c = dbh.getGestures(Integer.parseInt(prediction.name));
                if(c.moveToFirst()){
                    final int action = c.getInt(1);
                    final String option = c.getString(2);
                    final String desc = c.getString(3);
                    final boolean finisAfterLaunch = g2lPref.getBoolean("finish_activity_after_launch", true);
                    boolean confirmationBeforeLaunch = false;
                    try{
                        confirmationBeforeLaunch = c.getString(4).equals("true");
                    }catch(Exception e){

                    }
                    dbh.close();
                    if(confirmationBeforeLaunch){
                        showConfirmation(action, option, desc, finisAfterLaunch);
                    }else{
                        launchActivity(action, option,desc, finisAfterLaunch);

                    }
                    return;
                }



            }
            Toast message = Toast.makeText(this, R.string.no_matching_gesture, Toast.LENGTH_SHORT);
            message.setGravity(Gravity.CENTER, 0, 0);
            message.show();
        }catch(Exception E){
            E.printStackTrace();
        }

    }

    private void showConfirmation(int action, String option, String desc,
                                  boolean finisAfterLaunch) {

        if(g2lPref.getBoolean("manual_confirmation", false)){
            showManualConfirmation(action, option, desc, finisAfterLaunch);
        }else{
            showAutoCloseConfirmation(action, option, desc, finisAfterLaunch);
        }

    }


    private void showManualConfirmation(final int action, final String option,
                                        final String desc, final boolean finisAfterLaunch){
        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(
                this,getCurrentTheme()));
        alert.setTitle(R.string.confirm);
        alert.setMessage(desc);
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                launchActivity(action, option,desc, finisAfterLaunch);
            }
        });
        alert.show();
    }

    private Handler mHandler = new Handler();

    @SuppressLint("NewApi")
    private void showAutoCloseConfirmation(final int action, final String option,
                                           final String desc, final boolean finisAfterLaunch){




        final ProgressDialog confirmationDialog = new ProgressDialog(new ContextThemeWrapper(this,
                getCurrentProgressTheme()));
        confirmationDialog.setCancelable(true);
        confirmationDialog.setMax(100);
        confirmationDialog.setProgress(0);
        confirmationDialog.setIndeterminate(false);
        confirmationDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        confirmationDialog.setTitle(R.string.confirm);
        confirmationDialog.setMessage(desc);
        confirmationDialog.setProgressNumberFormat(null);
        confirmationDialog.setProgressPercentFormat(null);
        confirmationDialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                canceled = true;
                progressThread.interrupt();

            }
        });
        try{
            confirmationDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.cancel), this);
            confirmationDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.ok), this);
        }catch(Exception e){
            e.printStackTrace();
        }
        canceled = false;
        progressThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int progress = confirmationDialog.getProgress();
                int wait = g2lPref.getInt("auto_launch_wait_time", 5)*10;
                try{
                    while(progress < 100){
                        Thread.sleep(wait);
                        progress += 1;
                        final int progressx = progress;
                        mHandler.post(new Runnable() {
                            public void run() {
                                confirmationDialog.setProgress(progressx);
                            }
                        });


                    }
                }catch(Exception e){

                }

                confirmationDialog.dismiss();
                if(!canceled){
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            launchActivity(action, option,desc, finisAfterLaunch);
                        }
                    });
                }

            }
        });
        progressThread.start();
        confirmationDialog.show();
    }

    private void launchActivity(int action, String option, String desc, boolean finisAfterLaunch) {

        showToastIfNeeded(desc);

        switch(action){
            case GestureInfo.APPS:openApp(option,desc);break;
            case GestureInfo.WIFI:changeWifiState(option);break;
            case GestureInfo.BLUETOOTH:changeBluetoothState(option);break;
            case GestureInfo.DATA_CONNECTIVITY:changeDataState(option);break;
            case GestureInfo.SYNC:changeSyncState(option);break;
            case GestureInfo.BRIGHTNESS:changeBrightnessState(option);break;
            case GestureInfo.SOUND:changeSoundState(option);break;
            case GestureInfo.LED_FLASH:
                changeFlashStateIfPermissionGranted(option);break;
            case GestureInfo.OPEN_FILE:openFile(option);break;
            case GestureInfo.CALL:
                callIfPermissionGranted(option);break;
            case GestureInfo.MSG:openMessage(option);break;
            case GestureInfo.MAIL:openEmail(option);break;
            case GestureInfo.URL:openURL(option);break;
        }
        if(finisAfterLaunch && !waitingForPermission){
            finish();
        }

    }

    private void showToastIfNeeded(String desc){
        if(g2lPref.getBoolean("show_toast", true))
            Toast.makeText(getApplicationContext(), desc, Toast.LENGTH_SHORT).show();
    }

    private void openApp(String option, String desc) {



        try{
            Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
            String packageName = option.split(",")[0];
            String activityName = "";
            if(option.split(",").length >= 2){
                activityName = option.split(",")[1];
            }
            launchIntent.setComponent(new ComponentName(packageName, activityName));
            launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(launchIntent);
        }catch(Exception E){
            Toast.makeText(getApplicationContext(),
                    R.string.application_not_found, Toast.LENGTH_SHORT).show();
            System.out.println("Error"+E);
        }

    }

    private void changeWifiState(String val) {
        WifiManager wifiManager = (WifiManager)this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        boolean currentState = wifiManager.isWifiEnabled();
        boolean enable = currentState;
        int option = Integer.parseInt(val);
        switch(option){
            case DISABLE: enable = false;break;
            case ENABLE: enable = true;break;
            case TOGGLE: enable = !currentState;break;
        }
        if(enable != currentState){
            wifiManager.setWifiEnabled(enable);
        }

    }

    private void changeBluetoothState(String val) {
        

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean enabled = mBluetoothAdapter.isEnabled();
        int type =Integer.parseInt(val);
        if(type == TOGGLE){
            if(enabled){
                type = DISABLE;
            }else{
                type = ENABLE;
            }
        }
        switch(type){
            case DISABLE: if(enabled) { mBluetoothAdapter.disable();} break;
            case ENABLE:	if(!enabled) { mBluetoothAdapter.enable(); } break;
            case 3: Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120);
                startActivity(discoverableIntent);break;
        }

    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void changeDataState(String val) {
        int type =Integer.parseInt(val);
        ConnectivityManager connec = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        boolean currentState;

        currentState = mobile.isConnected();

        boolean enable = currentState;
        switch(type){
            case DISABLE: enable = false;break;
            case ENABLE: enable = true;break;
            case TOGGLE: enable = !currentState;break;
        }
        if(enable != currentState){
            final ConnectivityManager conman = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            Class conmanClass;
            try {
                conmanClass = Class.forName(conman.getClass().getName());

                java.lang.reflect.Field iConnectivityManagerField;

                iConnectivityManagerField = conmanClass.getDeclaredField("mService");
                iConnectivityManagerField.setAccessible(true);
                final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
                final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                setMobileDataEnabledMethod.setAccessible(true);

                setMobileDataEnabledMethod.invoke(iConnectivityManager, enable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }

    private void changeSyncState(String val) {
        int type = Integer.parseInt(val);
        boolean currentState = ContentResolver.getMasterSyncAutomatically();
        boolean enable = currentState;
        switch(type){
            case DISABLE: enable = false;break;
            case ENABLE: enable = true;break;
            case TOGGLE: enable = !currentState;break;
        }
        if( enable != currentState ){
            ContentResolver.setMasterSyncAutomatically(enable);
        }

    }

    private void changeBrightnessState(String val) {
        int type =Integer.parseInt(val);
        int brightness = 127;
        try {
            brightness = android.provider.Settings.System.getInt(
                    getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (SettingNotFoundException e) {
            
            e.printStackTrace();
        }
        switch(type){
            case 1:brightness = 0;break;
            case 2:brightness = 127;break;
            case 3:brightness = 255;break;
            case TOGGLE:
                if(brightness >= 0 && brightness < 127){
                    brightness = 127;
                }else if(brightness >= 127 && brightness < 255){
                    brightness = 255;
                }else if(brightness == 255){
                    brightness = 0;
                }
                break;

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(getApplicationContext())) {
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(intent, 1000 + brightness);
                waitingForPermission = true;
                return;
            }

        }
        android.provider.Settings.System.putInt(getApplicationContext().getContentResolver(),
                android.provider.Settings.System.SCREEN_BRIGHTNESS, brightness);

    }



    private void changeSoundState(String val) {
        int type =Integer.parseInt(val);
        int mode = 1;
        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        switch(type){
            case 1:am.setRingerMode(AudioManager.RINGER_MODE_SILENT);break;
            case 2:am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);break;
            case 3:am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);break;
            case TOGGLE:mode = am.getRingerMode();
                if(mode == AudioManager.RINGER_MODE_SILENT ){
                    am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);break;
                }else{
                    am.setRingerMode(AudioManager.RINGER_MODE_SILENT);break;
                }
        }

    }


    @TargetApi(23)
    private void changeFlashStateIfPermissionGranted(String val) {
        if(!isPermissionGranted(Manifest.permission.CAMERA)) {
            FLASH_STATE = val;
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQ_CAMERA);
            waitingForPermission = true;
            return;
        }
        changeFlashState(val);

    }


    private void changeFlashState(String val) {
        int action = Integer.parseInt(val);
        if(action == TOGGLE){
            if(FlashSettingns.flashOn){	changeFlashStateIfPermissionGranted(DISABLE+"");return; }
            else{	changeFlashStateIfPermissionGranted(ENABLE+"");return; }
        }
        PackageManager pm = this.getPackageManager();

        // if device support camera?
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e("err", "Device has no camera!");
            return;
        }
        try{


            if(action==DISABLE){
                FlashSettingns.turnOffFlash();
            }else if(action == ENABLE){
                FlashSettingns.turnOnFlash();
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void openFile(String path) {
        File file = new File(path);
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        int index = file.getName().lastIndexOf('.')+1;
        String ext = "";
        String type = "*/*";
        try{
            ext = file.getName().substring(index).toLowerCase(Locale.getDefault());
            type = mime.getMimeTypeFromExtension(ext);
        }catch(Exception E){
            type = "*/*";
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.fromFile(file);

        intent.setDataAndType(data, type);

        startActivity(intent);
    }

    private void callIfPermissionGranted(String no) {

        PHONE_NO = no;
        if(checkCallPermission()) {
            call(no);
            return;
        }
        waitingForPermission = true;
        return;

    }


    private void call(String no) {
        try{
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+ no));
            startActivity(callIntent);

        }catch(Exception E){
            //System.out.println("Error"+E);
        }
    }

    private void openMessage(String no) {

        Uri uri = Uri.parse("smsto:" + no);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", "");
        startActivity(intent);

    }

    private void openEmail(String email) {

        try{
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            startActivity(Intent.createChooser(emailIntent, getString(R.string.send_mail)));
        }catch(Exception E){
            //System.out.println("Error"+E);
        }

    }

    private void openURL(String url) {

        try{
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if(!(url.startsWith("http://")||url.startsWith("https://")||url.startsWith("ftp://"))){
                url = "http://"+url;
            }
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }catch(Exception E){
            //System.out.println("Error"+E);
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        if(which == DialogInterface.BUTTON_NEGATIVE ||
                which == DialogInterface.BUTTON_POSITIVE){
            if(which == DialogInterface.BUTTON_NEGATIVE)	canceled = true;
            progressThread.interrupt();
        }

        //myPd_ring.dismiss();

    }

    @TargetApi(23)
    private boolean checkCallPermission() {

        String permissions[] = new String[]{Manifest.permission.CALL_PHONE};
        if(isPermissionGranted(permissions[0])) {
            return true;
        }
        requestPermissions(permissions, 11);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_STORAGE && isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {

            File storeFile = new File(appPath, "gestures");
            if (!storeFile.exists()) {
                try {
                    storeFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            gestureLib = GestureLibraries.fromFile(storeFile);
            gestureLib.load();
        }else if(requestCode == REQUEST_STORAGE) {
            Toast.makeText(getApplicationContext(), R.string.storage_permission_required,
                    Toast.LENGTH_SHORT).show();
        }
        else if(requestCode == REQ_CAMERA && isPermissionGranted(Manifest.permission.CAMERA)) {
            changeFlashState(FLASH_STATE);
            if(g2lPref.getBoolean("finish_activity_after_launch", true)) {
                finish();
            }
        }
        else if (isPermissionGranted(Manifest.permission.CALL_PHONE) && requestCode == 11) {
            call(PHONE_NO);
            if(g2lPref.getBoolean("finish_activity_after_launch", true)) {
                finish();
            }
        }

    }

    private int getCurrentProgressTheme() {
        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        return pref.getBoolean("enable_dark_theme", false) ? R.style.EAProgressDialog_Dark:
                R.style.EAProgressDialog_Light ;
    }

    private int getCurrentTheme() {
        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        return pref.getBoolean("enable_dark_theme", false) ? R.style.Dialog_Dark:
                R.style.Dialog_Light;
    }




}

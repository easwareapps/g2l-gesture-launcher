package com.easwareapps.g2l;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class PermissionRequestActivity extends AppCompatActivity {

    public static final int REQ_OVERLAY_PERMISSION = 888;
    public static final int REQ_OVERLAY_PERMISSION_FINISH = 889;
    Button toggleOverlayPermission;
    Button requestPermission;
    int index = 0;
    String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.CAMERA
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
                R.style.G2LTheme_NoActionBar ;
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission_request);
        toggleOverlayPermission = (Button) findViewById(R.id.toggleOverlayPermission);
        requestPermission = (Button) findViewById(R.id.requestPermissions);
        changeText();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        
        if(toolbar != null) {
            toolbar.setTitle(getResources().getString(R.string.fix_permission_issues));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationIcon(R.mipmap.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    public void requestOverlayPermission(View v) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(intent, REQ_OVERLAY_PERMISSION);
    }

    public void requestPermissions(View v) {
        requestNextPermission();

    }

    private void requestNextPermission() {
        if(index >= permissions.length) {
            return;
        }
        checkPermission(permissions[index]);
        index++;
    }

    @TargetApi(23)
    private boolean checkPermission(String permission) {


        if(isPermissionGranted(permission)) {
            return true;
        }
        requestPermissions(new String[]{permission}, 11);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        requestNextPermission();
    }

    private void changeText() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(getApplicationContext())) {
                toggleOverlayPermission.setText(R.string.revoke_overlay_permissions);
            } else {
                toggleOverlayPermission.setText(R.string.grant_overlay_permissions);
            }
        }

        boolean granted = true;
        for (String permission : permissions) {
            if(!isPermissionGranted(permission)) {
                granted = false;
            }
        }
        if(granted) {
            requestPermission.setEnabled(false);
        }
    }

    public void showAppSettings(View v) {

        Intent intent = new Intent();
        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        changeText();
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}

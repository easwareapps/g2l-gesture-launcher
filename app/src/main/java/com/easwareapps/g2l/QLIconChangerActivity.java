/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class QLIconChangerActivity extends AppCompatActivity {

    Toolbar toolbar;
    static final int IMAGE_SELECT_CODE = 777;
    CropImageView cropImageView;
    AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
                R.style.G2LTheme_NoActionBar ;
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qlicon_changer);

        
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null) {
            toolbar.setTitle(getResources().getString(R.string.title_ql_icon_changer));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationIcon(R.mipmap.ic_back);
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }



        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        cropImageView.setAspectRatio(1, 1);
        cropImageView.setCropShape(CropImageView.CropShape.OVAL);




    }

    private void setPreview() {
        Bitmap cropped = cropImageView.getCroppedImage();
        //preview.setImageBitmap(getRoundImage(cropped));
        if (saveBitmap(getRoundImage(cropped))) {
            SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("custom_quick_launch_icon", true);
            editor.apply();
            finish();
        }

    }

    public Bitmap getRoundImage(Bitmap bitmap){


        if(bitmap == null){
            return  null;
        }
        try {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            paint.setDither(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.parseColor("#BAB399"));
            canvas.drawCircle(bitmap.getWidth() / 2 + 0.7f, bitmap.getHeight() / 2 + 0.7f,
                    bitmap.getWidth() / 2 + 0.1f, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            bitmap.recycle();
            return output;
        }catch (Exception e){
            try {
                if(bitmap != null)
                    bitmap.recycle();
            } catch (Exception e1) {

            }
            e.printStackTrace();
        }

        return  null;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ql_icon_change, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_change_icon) {
            selectQuickLaunchIcon();
        } else  if (id == R.id.menu_done) {
            setPreview();
        } else  if (id == R.id.menu_reset) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.confirm);
            builder.setMessage(R.string.reset_ql_icon).setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean("custom_quick_launch_icon", false);
                    editor.apply();
                    dialog.dismiss();
                    finish();
                }
            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                  dialog.dismiss();
                }
            });
            dialog = builder.create();
            dialog.show();


        }
        return true;
    }

    public void selectQuickLaunchIcon() {
        if(!isStoragePermissionGranted(IMAGE_SELECT_CODE)) {
            return;
        }
        selectImage();
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        //intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, getResources().getText(R.string.select_icon)),
                    IMAGE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getApplicationContext(), getResources().getText(R.string.install_file_manager),
                    Toast.LENGTH_SHORT).show();
        }
    }



    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                int idx = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                String path = cursor.getString(idx);
                cursor.close();
                return path;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uri.getPath();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == IMAGE_SELECT_CODE && resultCode == Activity.RESULT_OK) {


            Uri photoUri = data.getData();
            String path = getRealPathFromURI(photoUri);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            Bitmap photo = BitmapFactory.decodeFile(path);
            opts.inSampleSize = (photo.getWidth() > getWidth()) ? photo.getWidth()/getWidth():1;
            photo.recycle();
            opts.inJustDecodeBounds = false;
            photo = BitmapFactory.decodeFile(path, opts);
            cropImageView.setImageBitmap(photo);
            cropImageView.setAspectRatio(1, 1);
            cropImageView.setFixedAspectRatio(true);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @TargetApi(23)
    private boolean isStoragePermissionGranted(int code) {

        String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(isPermissionGranted(permissions[0])) {
            return true;
        }
        requestPermissions(permissions, code);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            selectImage();
        }
    }

    public boolean saveBitmap(Bitmap bitmap){

        File storageDir = new File(Environment.getExternalStorageDirectory(), ".g2l");
        if(!storageDir.exists()){
            if(!storageDir.mkdirs()){
                Toast.makeText(getApplicationContext(),
                        "Sorry Can't make cache dir", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        File filename = new File(storageDir, "quick_launcher.png");
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            filename.setReadable(true);
            //bitmap.recycle();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;


    }


    public int getWidth() {
        WindowManager wm = (WindowManager) getApplicationContext()
                            .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return  size.x;

    }
}

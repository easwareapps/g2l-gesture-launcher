/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;



public class DonateActivity extends AppCompatActivity implements OnClickListener {

	Button btnOne = null;
	Button btnTwo = null;
	Button btnThree = null;
	Button btnFour = null;
	ImageView imgThankyou = null;
	TextView txtPaymentDetails;
	private Toolbar toolbar;





	@Override
	protected void onCreate(Bundle savedInstanceState) {
		SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
				R.style.G2LTheme_NoActionBar ;
		setTheme(theme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.donate_activity);
		try{
			Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
			serviceIntent.setPackage("com.android.vending");
			//this.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
		}catch(Exception e){

		}

		
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		if(toolbar != null) {
			toolbar.setTitle(getResources().getString(R.string.title_donate));
			toolbar.setTitleTextColor(Color.WHITE);
			toolbar.setNavigationIcon(R.mipmap.ic_back);
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					finish();
				}
			});
		}

		btnOne = (Button)findViewById(R.id.btnOptionOne);
		btnTwo = (Button)findViewById(R.id.btnOptionTwo);
		btnThree = (Button)findViewById(R.id.btnOptionThree);
		btnFour = (Button)findViewById(R.id.btnOptionFour);
		imgThankyou = (ImageView)findViewById(R.id.imgThankyou);
		txtPaymentDetails = (TextView)findViewById(R.id.txtPaymentDetails);
		SharedPreferences g2lPref = this.getSharedPreferences(getPackageName(), MODE_PRIVATE);
		if(g2lPref.getBoolean("donated", false)){
			try {
				setThankyouImage();
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

		btnOne.setOnClickListener(this);
		btnTwo.setOnClickListener(this);
		btnThree.setOnClickListener(this);
		btnFour.setOnClickListener(this);



	}

	private void setThankyouImage() throws Exception{
		new Thread(new Runnable() {
			@Override
			public void run() {
				final Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.thankyou);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						imgThankyou.setImageBitmap(image);
					}
				});
			}
		}).start();


	}



	@Override
	public void onClick(View v) {

		try {
			if (v == btnOne) {
				openLink("http://g2l.easwareapps.com?donate=1");
				setThankyouImage();
			} else if (v == btnTwo) {
				openLink("http://g2l.easwareapps.com?donate=2");
				setThankyouImage();
			} else if (v == btnThree) {
				openLink("http://g2l.easwareapps.com?donate=3");
				setThankyouImage();
			} else if (v == btnFour) {
				openLink("http://g2l.easwareapps.com?donate=4");
				setThankyouImage();
			}
		}catch (Exception e) {

		}



	}


	private void openLink(String url){

		Intent browserIntent = new Intent(Intent.ACTION_VIEW);
		browserIntent.setData(Uri.parse(url));
		startActivity(browserIntent);
	}





}

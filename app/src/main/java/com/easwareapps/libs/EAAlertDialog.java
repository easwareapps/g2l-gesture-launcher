/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.libs;

import android.app.AlertDialog;
import android.content.Context;

public class EAAlertDialog {

	Context appContext = null;
	public EAAlertDialog(Context context) {
		appContext = context;
	}
	
	public AlertDialog.Builder getAlertDialog(String title, String msg, boolean cancelable){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(appContext);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(cancelable);
		return builder;
	}

}

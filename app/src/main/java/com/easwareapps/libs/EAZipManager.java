/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.libs;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import android.util.Log;

public class EAZipManager {

	public void zip(String[] _files, String zipFileName, String dir) {
		int BUFFER = 2048;
		try {
			BufferedInputStream origin = null;
			FileOutputStream dest = new FileOutputStream(zipFileName);
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
					dest));
			byte data[] = new byte[BUFFER];

			for (int i = 0; i < _files.length; i++) {
				Log.v("Compress", "Adding: " + _files[i]);
				FileInputStream fi = new FileInputStream(_files[i]);
				origin = new BufferedInputStream(fi, BUFFER);
				//substring(dir.getAbsolutePath().length()+1, filePath.length()));
				ZipEntry entry = new ZipEntry(_files[i].substring(dir.length()+1, _files[i].length()));
				out.putNextEntry(entry);
				int count;

				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void unzip(String zipFile, String targetLocation) {
		
		if(!targetLocation.endsWith("/")){
			targetLocation += "/";
		}
		dirChecker(targetLocation);

		try {
			FileInputStream fin = new FileInputStream(zipFile);
			ZipInputStream zin = new ZipInputStream(fin);
			ZipEntry ze = null;
			byte[] buffer = new byte[1024];
	        int count;
			while ((ze = zin.getNextEntry()) != null) {
				if (ze.isDirectory()) {
					Log.d("DIR", ze.getName());
					dirChecker(targetLocation+ze.getName());
				}
				else{
					String path[] = ze.getName().split("/");
					String s = "";
					for(int i=0;i<path.length-1;i++){
						s += path[i] + "/";
					}
					if(s!=""){
						dirChecker(targetLocation + File.separator + s);
					}
					FileOutputStream fout = new FileOutputStream(new File(targetLocation, ze.getName()));
					while ((count = zin.read(buffer)) != -1) {
		                fout.write(buffer, 0, count);             
		            }
					zin.closeEntry();
					fout.close();
				}
			}
			zin.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void dirChecker(String dirPath){
		File dir = new File(dirPath);
		if(!dir.exists()){
			Log.d("ZM", "creating directory" + dirPath);
			dir.mkdir();
		}else{
			Log.d("ZM", "no need to create directory" + dirPath);
			
		}
	}

}

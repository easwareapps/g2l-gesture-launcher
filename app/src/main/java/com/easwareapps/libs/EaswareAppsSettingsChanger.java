/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.libs;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

public class EaswareAppsSettingsChanger {

	Context context = null;
	
	public static int SILENT = AudioManager.RINGER_MODE_SILENT;
	public static int VIBRATE = AudioManager.RINGER_MODE_VIBRATE;
	public static int NORMAL = AudioManager.RINGER_MODE_NORMAL;

	public EaswareAppsSettingsChanger(Context c) {

		context = c;
	}

	/*
	 * Get Settings state
	 */
	public boolean getWifiState() {
		WifiManager wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		return wifiManager.isWifiEnabled();

	}

	public boolean getDataState() {
		ConnectivityManager connec = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobile = connec
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		return mobile.isConnected();
	}

	public int getSoundState(){

		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		return am.getRingerMode();
	}

	/*
	 * Set Settings state
	 */
	public void setWifiState(boolean state) {
		if(getWifiState() == state)	return;
		WifiManager wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		wifiManager.setWifiEnabled(state);

	}

	@SuppressWarnings("rawtypes")
	public void setDataState(boolean state) {
		if(getDataState() == state)	return;

		final ConnectivityManager conman = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		Class conmanClass;
		try {
			conmanClass = Class.forName(conman.getClass().getName());

			Field iConnectivityManagerField;

			iConnectivityManagerField = conmanClass
					.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			final Object iConnectivityManager = iConnectivityManagerField
					.get(conman);
			final Class iConnectivityManagerClass = Class
					.forName(iConnectivityManager.getClass().getName());
			@SuppressWarnings("unchecked")
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass
			.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);

			setMobileDataEnabledMethod.invoke(iConnectivityManager, state);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void setSoundState(int state){
		if(getSoundState() == state)	return;
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		am.setRingerMode(state);
	}

	@SuppressWarnings("rawtypes")
	public void setGPSState(boolean state) throws ClassNotFoundException, NoSuchFieldException{
		LocationManager mLocationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE); 
		Class c = Class.forName(mLocationManager.getClass().getName());  
		Field f = c.getDeclaredField("mService");  
		f.setAccessible(state);  

	}
}
